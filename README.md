## TODO

- IOS notifications



## GraphQL Gist
### Update current cache ->
Use `readQuery` to read some queries you called before. Then you can write it again to override cache snapshot of this query.<br>
Use `readFragment` to get an object stored in cache, regardless of which query provided it's value. Then you can use `writeFragment` to update this fragment query value.

### Local cache data (states) ->
Use `writeData` to write local cache.