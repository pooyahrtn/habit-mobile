module.exports = {
  client: {
    service: {
      name: 'happit',
      url: 'http://localhost:3000/graphql',
      // optional headers
      // headers: {
      //   authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZGNiYmUxZDU2ZjZhMjAyYjNkZjhhNzQiLCJpYXQiOjE1NzM2MzM1NjUsImV4cCI6MTU3MzgwNjM2NX0.ckmvYr07Xhc6P9JBTp7AHktfPjsbstPBQv9d6Mfteqw',
      // },
      // optional disable SSL validation check
      skipSSLValidation: true,
    },
  },
};
