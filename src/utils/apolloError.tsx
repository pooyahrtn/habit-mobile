import { ApolloError } from 'apollo-boost';
interface InlineError {
  message: string;
}

export const getInlineError = (error: ApolloError): InlineError[] => {
  return error.graphQLErrors
    .map((e) => e.message)
    .filter((e) => typeof e === 'object')
    .filter((e: any) => e.type === 'inline')
    .map((e: any) => ({ message: e.message }));
};
