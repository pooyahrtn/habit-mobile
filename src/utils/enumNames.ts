import { Toughness, RemindTime } from '@graphql/types/graphql-global-types';

export const toughnessNames = (toughness: Toughness): string => {
  switch (toughness) {
    case Toughness.Easy:
      return 'آسون';
    case Toughness.Medium:
      return 'متوسط';
    case Toughness.Hard:
      return 'سخت';
  }
};

export const remindTimeNames = (remindTime: RemindTime): string => {
  switch (remindTime) {
    case RemindTime.Any:
      return 'هروقت';
    case RemindTime.Evening:
      return 'عصر';
    case RemindTime.Morning:
      return 'صبح';
    case RemindTime.Night:
      return 'شب';
    case RemindTime.Afternoon:
      return 'ظهر';
  }
};
