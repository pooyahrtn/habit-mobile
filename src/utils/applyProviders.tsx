import React from 'react';
type HOC = (Children: any) => any;

export function applyProviders(Children: any, hocs: HOC[] = []): any {
  if (hocs.length === 0) {
    return (props: any) => <Children {...props} />;
  }
  const [hoc, ...restHocs] = hocs;
  return applyProviders(hoc(Children), restHocs);
}
