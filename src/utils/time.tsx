export const hoursToMS = (hours: number): number => 1000 * 60 * 60 * hours;
