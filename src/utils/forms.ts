export interface FormItem<S = {}> {
  key: keyof S;
  title: string;
  description?: string;
  type: 'text' | 'group' | 'radio';
  errors?: (value: string) => string[] | undefined;
  text?: {
    placeholder: string;
    unit?: string;
    numberOfLines?: number;
    inputType: 'string' | 'number';
    minLength?: number;
    maxLength?: number;
  };
  group?: {
    items: {
      key: any;
      value: string;
    }[];
  };
  radio?: {
    items: {
      key: any;
      value: string;
    }[];
  };
}

// export const mapError = (items: FormItem[]): FormItem[] => {
//   return items.map((i) => {
//     if (i.value) {
//       return { ...i, hasError: false };
//     }
//     return { ...i, hasError: true };
//   });
// };

// export const anyHasError = (items: FormItem[]): boolean =>
//   items.some((i) => i.hasError);
