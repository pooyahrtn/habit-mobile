import { toEnglish } from './text';
import { toPersian } from './text';
import R from 'ramda';
export const normalizePhone = (phone: string): string => {
  return R.pipe(
    (p: string) => p.substring(1),
    (p: string) => '98' + p,
    toEnglish,
  )(phone);
};

/**
 * @example 989124594688 -> 09124594688
 * @param phone : string
 */
export const beutifyPhone = (phone: string): string => {
  return R.pipe(
    (p: string) => p.substring(2),
    (p: string) => `0${p}`,
    toPersian,
  )(phone);
};
