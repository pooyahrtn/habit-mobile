export type SetCallback<T> = (value: T) => void;
