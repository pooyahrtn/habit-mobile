import React, { useState, useEffect } from 'react';
import { StatusBar } from 'react-native';
StatusBar.setBackgroundColor('rgba(100,100,100,0)');
StatusBar.setBarStyle('dark-content');
StatusBar.setTranslucent(true);
import {
  getClient,
  ApolloClientConfig,
  ApolloClientType,
} from '../packages/apollo';
import { initData } from '../graphql/localData';
import { typeDefs } from '../graphql/localDefs';
import { ApolloProvider } from '@apollo/react-hooks';
import SplashScreen from '../pages/splash/splash.page';
import OnAppLoad from './onAppLoad';
import { restore } from '../packages/localstorage';
import config from '../config';

const apolloConfig: ApolloClientConfig = {
  url: config.graphqlEndpoing,
  initData,
  localTypeDefs: typeDefs,
};

export default () => {
  const [appolloClient, setApolloClient] = useState<ApolloClientType>();
  const [localstorage, setLocalstorage] = useState<boolean>(false);

  useEffect(() => {
    getClient(apolloConfig).then(setApolloClient);
    restore().then(() => setLocalstorage(true));
  }, []);
  if (!appolloClient || !localstorage) {
    return <SplashScreen />;
  }
  return (
    <ApolloProvider client={appolloClient}>
      <OnAppLoad />
    </ApolloProvider>
  );
};
