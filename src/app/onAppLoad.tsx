import React from 'react';
import { View, StyleSheet } from 'react-native';
import Router from '../route';
import SplashScreen from '@pages/splash/splash.page';
import { AppEnterState } from '@graphql/types/graphql-global-types';
import { addOnAuthFailedObserver } from '../packages/apollo';
import { ToastWrapper } from '@components/Toast';
import { TaskCompleteModal } from '@pages/TaskCompleteModal';
import { usePrefetch } from '@controller/startup/usePrefetch';
import { useAppState } from '@controller/startup/useAppState';
import { useOpenApp } from '@controller/useOpenApp';
import config from '../config';

export default () => {
  const { appState, setAppState, loading } = useAppState();
  usePrefetch(appState ?? AppEnterState.JUST_ENTER);
  addOnAuthFailedObserver(() => {
    setAppState(AppEnterState.WAIT_FOR_LOGIN);
  });
  useOpenApp({ androidVersion: config.androidVersion });

  if (loading) {
    return <SplashScreen />;
  }
  console.warn('onAppLoad');
  return (
    <View style={styles.wrapper}>
      <ToastWrapper>
        <TaskCompleteModal>
          <Router
            entry={{
              appEnterMode: appState ?? AppEnterState.JUST_ENTER,
            }}
          />
        </TaskCompleteModal>
      </ToastWrapper>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});
