import { useApolloClient } from '@apollo/react-hooks';
import { AppEnterState } from '@graphql/types/graphql-global-types';
import { startupQuery } from '@controller/challenge/useStartupChallenges';
import { challengeQuery } from '@controller/challenge/useGetChallenges';
import { leaderboardQuery } from '@controller/useLeaderboard';
import { avatarQuery } from '@controller/useAvatars';

export const usePrefetch = (data: AppEnterState) => {
  const client = useApolloClient();

  if (
    !data ||
    data === AppEnterState.JUST_ENTER ||
    data === AppEnterState.WAIT_FOR_LOGIN
  ) {
    return;
  }
  if (data === AppEnterState.WAIT_FOR_PROFILE) {
    client.query({
      query: startupQuery.query,
      variables: startupQuery.variable,
    });
  } else {
    console.log('prefetch challenges');
    client.query({
      query: challengeQuery.query,
      variables: challengeQuery.variables,
      fetchPolicy: 'network-only',
    });
  }
  console.log('prefetch leaderboard');
  client.query({
    query: leaderboardQuery.query,
    fetchPolicy: 'network-only',
  });
  console.log('prefetch avatars');
  client.query({
    query: avatarQuery.query,
    fetchPolicy: 'network-only',
  });
};
