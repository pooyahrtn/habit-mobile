import { useCallback } from 'react';
import { useWriteData } from '@controller/utils';
import { AppEnterState } from '@graphql/types/graphql-global-types';
import { AppEnterStateQuery } from '@graphql/queries/__generated__/AppEnterStateQuery';
import enterStateQuery from '@graphql/queries/AppEnterState';
import { useQuery } from '@apollo/react-hooks';

export const useAppState = () => {
  const { loading, data } = useQuery<AppEnterStateQuery>(enterStateQuery);
  const write = useWriteData();
  const setAppState = useCallback(
    (state: AppEnterState) => {
      write({ appEnterState: state });
    },
    [write],
  );
  return { setAppState, appState: data?.appEnterState, loading };
};
