import REPORT_CHALLENGE from '@graphql/mutations/ReportChallenge';
import { useMutation } from '@apollo/react-hooks';
import analytics from '@react-native-firebase/analytics';
import {
  RerportChallenge,
  RerportChallengeVariables,
} from '@graphql/mutations/__generated__/RerportChallenge';
import { useCallback } from 'react';

export const useReportChallenge = () => {
  const [request] = useMutation<RerportChallenge, RerportChallengeVariables>(
    REPORT_CHALLENGE,
  );
  const report = useCallback(
    (challengeId: string) => {
      analytics().logEvent('report_challenge', { challengeId });
      return request({ variables: { input: { challengeId } } });
    },
    [request],
  );
  return { report };
};
