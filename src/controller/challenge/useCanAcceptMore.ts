import CAN_ACCESS_MORE from '@graphql/queries/CanAcceptMoreChallenge';
import { useQuery } from '@apollo/react-hooks';
import { CanAcceptMoreChallenges } from '@graphql/queries/__generated__/CanAcceptMoreChallenges';

export const useCanAcceptMore = () => {
  const { data } = useQuery<CanAcceptMoreChallenges>(CAN_ACCESS_MORE);
  return {
    canAcceptMore: data?.canAcceptMore ?? true,
  };
};
