import CHALLENGE_QUERY from '@graphql/queries/Challenges';
import {
  Challenges,
  ChallengesVariables,
} from '@graphql/queries/__generated__/Challenges';
import { useQuery } from '@apollo/react-hooks';

export const useStartupChallenges = () => {
  const { loading, data } = useQuery<Challenges, ChallengesVariables>(
    startupQuery.query,
    {
      fetchPolicy: 'cache-and-network',
      variables: startupQuery.variable,
    },
  );
  return { loading, data };
};

export const startupQuery = {
  query: CHALLENGE_QUERY,
  variable: {
    filter: {
      count: 3,
      firstChallenges: true,
    },
  },
};
