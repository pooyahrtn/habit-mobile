import CANCEL_CHALLENGE from '@graphql/mutations/DeleteChallenge';
import CAN_ACCESS_MORE from '@graphql/queries/CanAcceptMoreChallenge';
import MY_SCHEULE from '@graphql/queries/MySchedule';
import { useMutation } from '@apollo/react-hooks';
import analytics from '@react-native-firebase/analytics';

import {
  CancelChallenge,
  CancelChallengeVariables,
} from '@graphql/mutations/__generated__/CancelChallenge';
import { useCallback } from 'react';

export const useCanceChallenge = () => {
  const [request, { loading }] = useMutation<
    CancelChallenge,
    CancelChallengeVariables
  >(CANCEL_CHALLENGE, {
    refetchQueries: [{ query: MY_SCHEULE }, { query: CAN_ACCESS_MORE }],
  });
  const cancelChallenge = useCallback(
    (challengeId: string) => {
      analytics().logEvent('remove_challenge', { challengeId });
      return request({
        variables: {
          input: {
            challengeId,
          },
        },
      });
    },
    [request],
  );
  return { cancelChallenge, loading };
};
