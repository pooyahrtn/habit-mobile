import { useCallback } from 'react';
import ACCEPT_CHALENGE_MUTATTION from '@graphql/mutations/AcceptChallenge';
import CAN_ACCESS_MORE from '@graphql/queries/CanAcceptMoreChallenge';
import MY_SCHEDULE_QUERY from '@graphql/queries/MySchedule';
import { useMutation } from '@apollo/react-hooks';
import {
  AcceptChallenge,
  AcceptChallengeVariables,
} from '@graphql/mutations/__generated__/AcceptChallenge';
import analytics from '@react-native-firebase/analytics';

export const useAcceptChallenge = () => {
  const [request, { loading }] = useMutation<
    AcceptChallenge,
    AcceptChallengeVariables
  >(ACCEPT_CHALENGE_MUTATTION);
  const requestAcceptChallenge = useCallback(
    async (id: string) => {
      analytics().logEvent('challenge_accept');
      const { data } = await request({
        variables: { input: { challengeId: id } },
        refetchQueries: [
          { query: MY_SCHEDULE_QUERY },
          { query: CAN_ACCESS_MORE },
        ],
      });
      return data?.acceptChallenge;
    },
    [request],
  );

  return { loading, requestAcceptChallenge };
};
