import CREATE_CHALLENGE from '@graphql/mutations/CreateChallenge';
import { useMutation } from '@apollo/react-hooks';
import {
  CreateChallenge,
  CreateChallengeVariables,
} from '@graphql/mutations/__generated__/CreateChallenge';
import { CreateChallengeInput } from '@graphql/types/graphql-global-types';

export const useCreateChallenge = () => {
  const [request, { loading }] = useMutation<
    CreateChallenge,
    CreateChallengeVariables
  >(CREATE_CHALLENGE);

  const requestCreateChallenge = (data: CreateChallengeInput) => {
    return request({ variables: { input: data } });
  };
  return { requestCreateChallenge, loading };
};
