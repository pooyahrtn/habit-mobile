import CHALLENGE_QUERY from '@graphql/queries/Challenges';
import {
  Challenges,
  ChallengesVariables,
} from '@graphql/queries/__generated__/Challenges';
import { useQuery } from '@apollo/react-hooks';

export const useGetChallenges = () => {
  const { loading, data, refetch } = useQuery<Challenges, ChallengesVariables>(
    challengeQuery.query,
    {
      variables: challengeQuery.variables,
    },
  );
  return { loading, data, refetch };
};

export const challengeQuery = {
  query: CHALLENGE_QUERY,
  variables: {
    filter: {
      count: 100,
    },
  },
};
