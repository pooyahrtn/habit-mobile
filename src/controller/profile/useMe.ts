import ME_QUERY from '@graphql/queries/Me';
import { useQuery } from '@apollo/react-hooks';
import { Me } from '@graphql/queries/__generated__/Me';

/**
 * MMMMMM use me baby
 */
export const useMe = () => {
  const { data, loading } = useQuery<Me>(ME_QUERY, {
    fetchPolicy: 'cache-and-network',
  });
  return {
    me: data?.me,
    loading,
  };
};
