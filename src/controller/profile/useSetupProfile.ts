import { useCallback } from 'react';
import ME_QUERY from '@graphql/queries/Me';
import { Me } from '@graphql/queries/__generated__/Me';
import UPDATE_ME from '@graphql/mutations/UpdateProfile';
import { useMutation } from '@apollo/react-hooks';
import {
  UpdateUser,
  UpdateUserVariables,
} from '@graphql/mutations/__generated__/UpdateUser';
import {
  UpdateUserInput,
  AppEnterState,
} from '@graphql/types/graphql-global-types';
import { useWriteData } from '@controller/utils';
import analytics from '@react-native-firebase/analytics';

export const useSetupProfile = (config?: { firstSetup: boolean }) => {
  const [request, { loading, error }] = useMutation<
    UpdateUser,
    UpdateUserVariables
  >(UPDATE_ME, {
    update(cache, { data }) {
      if (data) {
        // console.log(data);
        cache.writeQuery<Me>({
          data: { me: data.updateMe },
          query: ME_QUERY,
        });
      }
    },
  });
  const writeData = useWriteData();
  const requestSetupUser = useCallback(
    async (data: UpdateUserInput) => {
      const res = await request({ variables: { input: data } });
      if (res.data?.updateMe && config?.firstSetup) {
        writeData({ appEnterState: AppEnterState.PICK_FIRST_CHALLENGE });
      }
    },
    [request, config, writeData],
  );

  const updateMe = useCallback(
    (data: UpdateUserInput) => {
      analytics().logEvent('update_profile');
      return request({ variables: { input: data } });
    },
    [request],
  );

  return { requestSetupUser, updateMe, loading, error };
};
