import AVATAR_QUERY from '@graphql/queries/AvatarImages';
import { useQuery } from '@apollo/react-hooks';
import { AvatarImages } from '@graphql/queries/__generated__/AvatarImages';

export const useAvatars = () => {
  const { data } = useQuery<AvatarImages>(avatarQuery.query);
  return {
    avatars: data?.avatarImages ?? [],
  };
};

export const avatarQuery = {
  query: AVATAR_QUERY,
};
