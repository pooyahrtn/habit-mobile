import OPEN_APP from '@graphql/mutations/OpenApp';
import { useMutation } from '@apollo/react-hooks';
import {
  OpenApp,
  OpenAppVariables,
} from '@graphql/mutations/__generated__/OpenApp';
import { useCallback, useEffect } from 'react';
import { registerNotifications } from '@packages/notification';
import { useAppState } from './startup/useAppState';
import { AppEnterState } from '@graphql/types/graphql-global-types';

interface Config {
  androidVersion: number;
}
let sent = false;
export const useOpenApp = ({ androidVersion }: Config) => {
  const [request, { loading }] = useMutation<OpenApp, OpenAppVariables>(
    OPEN_APP,
  );
  const { appState, setAppState } = useAppState();
  const requestSetFcmToken = useCallback(
    (fcmToken: string) => {
      return request({ variables: { input: { fcmToken } } });
    },
    [request],
  );
  const registerFcmToken = useCallback(async () => {
    const fcmToken = await registerNotifications();
    const { data } = await requestSetFcmToken(fcmToken);
    if (data && data.openApp.androidMinVersion > androidVersion) {
      setAppState(AppEnterState.FORCE_UPDATE);
    }
  }, [requestSetFcmToken, androidVersion, setAppState]);

  useEffect(() => {
    if (appState === AppEnterState.LOGGED_IN && !sent) {
      sent = true;
      registerFcmToken();
    }
  }, [appState, registerFcmToken, loading]);
  return { requestSetFcmToken };
};
