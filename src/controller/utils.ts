import { useCallback } from 'react';
import { useApolloClient } from '@apollo/react-hooks';
import { LocalData } from '@graphql/localData';
import moment from 'moment-jalaali';
import { Day } from '@graphql/types/graphql-global-types';

export const useWriteData = () => {
  const client = useApolloClient();
  const writeData = useCallback(
    (data: Partial<LocalData>) =>
      client.writeData<Partial<LocalData>>({ data }),
    [client],
  );
  return writeData;
};

export const getToday = (): Day => {
  const now = moment().day();
  const days = [
    Day.Sunday,
    Day.Monday,
    Day.Tuesday,
    Day.Wednesday,
    Day.Thursday,
    Day.Friday,
    Day.Saturday,
  ];
  return days[now];
};

export const areDaysEqual = (a: Date, b: Date) =>
  moment(a).day() === moment(b).day();
