import DO_TASK_MUTATION from '@graphql/mutations/DoTask';
import MY_PROFILE from '@graphql/queries/Me';
import MY_SCHEDULE from '@graphql/queries/MySchedule';
import { useMutation } from '@apollo/react-hooks';
import {
  DoTask,
  DoTaskVariables,
} from '@graphql/mutations/__generated__/DoTask';
import { useCallback } from 'react';
import { getToday } from '@controller/utils';
import { useTaskCompleteModal } from '@pages/TaskCompleteModal';
import analytics from '@react-native-firebase/analytics';

export const useDoTask = () => {
  const [request, { loading }] = useMutation<DoTask, DoTaskVariables>(
    DO_TASK_MUTATION,
    {
      refetchQueries: [{ query: MY_SCHEDULE }, { query: MY_PROFILE }],
    },
  );
  const { showCompleteModal } = useTaskCompleteModal();
  const doTask = useCallback(
    async (taskId: string) => {
      analytics().logEvent('do_task');
      const res = await request({
        variables: { input: { taskId, day: getToday() } },
      });
      const { data } = res;
      if (data) {
        const {
          doTask: { reward, completed, challengeName, challengeId },
        } = data;
        if (completed) {
          showCompleteModal({ reward, challengeName, challengeId });
        }
      }
    },
    [request, showCompleteModal],
  );
  return { doTask, loading };
};
