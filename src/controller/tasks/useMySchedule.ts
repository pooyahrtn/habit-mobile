import MY_SCHEDULE from '@graphql/queries/MySchedule';
import { useQuery } from '@apollo/react-hooks';
import { MySchedule } from '@graphql/queries/__generated__/MySchedule';
import { getToday, areDaysEqual } from '@controller/utils';

let lastFetch: Date;

export const useMySchedule = () => {
  const { data, loading, refetch } = useQuery<MySchedule>(MY_SCHEDULE, {
    fetchPolicy: 'cache-and-network',
  });
  lastFetch = new Date();
  const refreshIfneeded = () => {
    if (!areDaysEqual(lastFetch, new Date())) {
      refetch();
    }
  };
  return {
    tasks: data && getTodaysTasks(data)?.taskStatuses,
    loading,
    refreshIfneeded,
  };
};

const getTodaysTasks = (data: MySchedule) => {
  const today = getToday();
  return data.mySchedule.find((d) => d.day === today);
};
