import { useCallback, useState, useEffect } from 'react';
import CreateUserMutation from '../../graphql/mutations/CreateUser';
import {
  CreateUser,
  CreateUserVariables,
} from '../../graphql/mutations/__generated__/CreateUser';
import { useMutation } from '@apollo/react-hooks';
import { normalizePhone } from '../../utils/phone';
import { getInlineError } from '@utils/apolloError';

export const usePhoneRegister = () => {
  const [request, { error }] = useMutation<CreateUser, CreateUserVariables>(
    CreateUserMutation,
    {
      errorPolicy: 'all',
    },
  );
  const [nextTryState, setNextTryState] = useState<Date>();
  const [showNextTry, setShowNextTry] = useState<boolean>(false);

  // listener on next try to show resend
  useEffect(() => {
    let id: any;
    if (nextTryState) {
      id = setTimeout(() => {
        setShowNextTry(true);
      }, (nextTryState.getTime() - new Date().getTime()) / 2);
    }
    return () => {
      clearTimeout(id);
    };
  }, [nextTryState]);

  const registerPhone = useCallback(
    async (phone: string) => {
      const normalizedPhone = normalizePhone(phone);
      const { data } = await request({
        variables: {
          input: {
            phoneNumber: normalizedPhone,
          },
        },
      });
      if (data) {
        const {
          createUser: { nextTry },
        } = data;
        setNextTryState(new Date(nextTry));
      }
    },
    [request],
  );

  const registerErrors = error && getInlineError(error);
  return { registerPhone, registerErrors, showNextTry };
};
