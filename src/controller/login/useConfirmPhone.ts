import { useCallback } from 'react';
import CONFIRM_MUTAION from '../../graphql/mutations/ConfirmPhone';
import {
  ConfirmPhone,
  ConfirmPhoneVariables,
} from '../../graphql/mutations/__generated__/ConfirmPhone';
import { useMutation } from '@apollo/react-hooks';
import { getInlineError } from '@utils/apolloError';
import { setData } from '../../packages/localstorage';
import { normalizePhone } from '@utils/phone';
import { useWriteData } from '../utils';
import { AppEnterState } from '@graphql/types/graphql-global-types';
import analytics from '@react-native-firebase/analytics';

export const useConfirmPhone = () => {
  const [requset, { error }] = useMutation<ConfirmPhone, ConfirmPhoneVariables>(
    CONFIRM_MUTAION,
    {},
  );
  const confirmErrors = error && getInlineError(error);
  const writeLocal = useWriteData();
  const requestConfirm = useCallback(
    async (phoneNumber: string, confirmCode: string) => {
      const normalizedPhone = normalizePhone(phoneNumber);

      const res = await requset({
        variables: { input: { phoneNumber: normalizedPhone, confirmCode } },
      });
      if (res.data) {
        const {
          verifyPhone: { accessToken, refreshToken },
        } = res.data;
        await setData({ accessToken, refreshToken });
        writeLocal({ appEnterState: AppEnterState.WAIT_FOR_PROFILE });
        analytics().logLogin({ method: 'phone' });
      }
    },
    [requset, writeLocal],
  );

  return { requestConfirm, confirmErrors };
};
