import LEADER_BOARD_QUERY from '@graphql/queries/Leaderboard';
import { useQuery } from '@apollo/react-hooks';
import { Leaderboard } from '@graphql/queries/__generated__/Leaderboard';

export const useLeaderboard = () => {
  const { data, loading } = useQuery<Leaderboard>(leaderboardQuery.query);

  return {
    leaderboard: data?.leaderboard ?? [],
    loading,
  };
};

export const leaderboardQuery = {
  query: LEADER_BOARD_QUERY,
};
