import { useQuery } from '@apollo/react-hooks';
import TUTORIAL_QUERY from '@graphql/queries/Tutorial';
import {
  Tutorial,
  Tutorial_tutorial,
} from '@graphql/queries/__generated__/Tutorial';
import { initData } from '@graphql/localData';
import { useCallback } from 'react';
import { useWriteData } from './utils';

export const useToturial = () => {
  const { data } = useQuery<Tutorial>(TUTORIAL_QUERY, {
    returnPartialData: true,
  });
  const write = useWriteData();

  const setTutorial = useCallback(
    (t: keyof Tutorial_tutorial) => {
      return write({
        tutorial: {
          ...initData.tutorial,
          ...data?.tutorial,
          [t]: true,
        },
      });
    },
    [write, data],
  );
  return { tutorial: data, setTutorial };
};
