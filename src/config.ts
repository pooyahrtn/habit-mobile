interface Config {
  graphqlEndpoing: string;
  androidVersion: number;
}

const defaultConfig: Config = {
  graphqlEndpoing: 'https://api.pupaaap.ir/graphql',
  androidVersion: 1,
};

const localConfig: Config = {
  ...defaultConfig,
  graphqlEndpoing: 'http://localhost:3000/graphql',
  // graphqlEndpoing: 'http://192.168.43.20:3000/graphql',
};

export default defaultConfig;
