import React, { useState, useCallback, useReducer } from 'react';
import { NewChallengeView } from '@components/new-challenge/new-challenge.view';
import { useFocusEffect } from '@react-navigation/native';
import { BackHandler } from 'react-native';
import {
  Toughness,
  RemindTime,
  ChallengeCategory,
} from '@graphql/types/graphql-global-types';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';
import { useCreateChallenge } from '@controller/challenge/useCreateChallenge';
import { FormItem } from '@utils/forms';
import {} from 'formik';

export const NewChallengePage = ({
  navigation,
}: PageProps<MainStackParams, 'newChallenge'>) => {
  const [currentPage, setCurrentPage] = useState(0);
  const [state, dispatch] = useReducer(
    (prevState: State, newState: Partial<State>) => ({
      ...prevState,
      ...newState,
    }),
    initState,
  );

  const dispatchField = (key: keyof State) => (value: any) =>
    dispatch({ [key]: value });

  const { requestCreateChallenge, loading } = useCreateChallenge();

  const onDone = () => {
    requestCreateChallenge({
      ...state,
      repeatCount: +state.repeatCount,
    }).then(() => navigation.replace('challengeCreated', {}));
  };

  const onNextPressed = () => {
    if (currentPage < 3) {
      setCurrentPage(currentPage + 1);
    } else {
      onDone();
    }
  };
  const handleBack = useCallback(() => {
    if (currentPage > 0) {
      setCurrentPage(currentPage - 1);
      return true;
    }
    return false;
  }, [currentPage]);
  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', handleBack);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', handleBack);
    }, [handleBack]),
  );

  return (
    <NewChallengeView
      currentPage={currentPage}
      onNextPressed={onNextPressed}
      onBackPressed={handleBack}
      firstInput={firstForm}
      secondInput={secondForm}
      loading={loading}
      state={state}
      onStateChange={dispatchField}
    />
  );
};

interface State {
  name: string;
  goal: string;
  description: string;
  remindText: string;
  repeatCount: string;
  toughness: Toughness;
  remindTime: RemindTime;
  category: ChallengeCategory;
}

const initState: State = {
  name: '',
  goal: '',
  description: '',
  remindText: '',
  repeatCount: '',
  toughness: Toughness.Easy,
  remindTime: RemindTime.Any,
  category: ChallengeCategory.Travel,
};

const firstForm: FormItem<State>[] = [
  {
    key: 'name',
    title: 'اسم چالش (لازم)',
    description: 'یک اسم کوتاه برای چالشت انتخاب کن:',
    errors: (v: string) => {
      if (v.length < 5) {
        return ['حداقل ۵ کاراکتر'];
      }
    },
    type: 'text',
    text: {
      placeholder: 'اسم چالش مثلا: ۷ روز صبحانه ',
      inputType: 'string',
      maxLength: 50,
    },
  },
  {
    key: 'goal',
    title: 'هدف چالش (لازم)',
    errors: (v: string) => {
      if (v.length < 10) {
        return ['حداقل ۱۰ کاراکتر'];
      }
    },
    description: 'کمتر از ۲ خط، از هدف چالش بگو: ',
    type: 'text',
    text: {
      placeholder: 'هدف چالش رو بنویس، مثلا: یک هفته قراره صبحونه بخوریم',
      inputType: 'string',
      numberOfLines: 3,
      maxLength: 100,
    },
  },
  {
    key: 'description',
    title: 'توضیحات (لازم)',
    description: 'تو چند خط، کوتاه بنویس که این چالش چجوری قراره کمک کنه؟',
    errors: (v: string) => {
      if (v.length < 10) {
        return ['حداقل ۱۰ کاراکتر'];
      }
    },
    type: 'text',
    text: {
      placeholder: 'توضیحات، مثلا: ثابت شده خوردن صبحانه خیلی مفیده…',
      numberOfLines: 7,
      inputType: 'string',
      maxLength: 400,
    },
  },
];

const secondForm: FormItem<State>[] = [
  {
    key: 'remindText',
    title: 'یاداوری (لازم)',
    description: 'متنی که برای یادآوری باید نشون داده بشه رو بنویس:',
    type: 'text',
    errors: (v: string) => {
      if (v.length < 5) {
        return ['حداقل ۵ کاراکتر'];
      }
    },
    text: {
      placeholder: 'متن یادآوری، مثلا:  صبحانه بخور',
      inputType: 'string',
      maxLength: 100,
    },
  },
  {
    key: 'repeatCount',
    title: 'چند بار (لازم)',
    description: 'چند بار تو هفته باید انجام بشه؟ (بین ۲ تا ۷ بار)',
    errors: (v: string) => {
      if (+v < 2 || +v > 7) {
        return ['عددی بین ۲ تا ۷'];
      }
    },
    type: 'text',
    text: {
      placeholder: 'تعداد تکرار در هفته',
      unit: 'بار',
      inputType: 'number',
    },
  },
  {
    key: 'toughness',
    title: 'سختی (لازم)',
    description: 'چقدر این چالش سخته؟',
    type: 'group',
    group: {
      items: [
        {
          key: Toughness.Easy,
          value: 'آسون',
        },
        {
          key: Toughness.Medium,
          value: 'متوسط',
        },
        {
          key: Toughness.Hard,
          value: 'سخت',
        },
      ],
    },
  },
  {
    key: 'remindTime',
    title: 'زمان (لازم)',
    description: 'زمان مناسب انجام چالش کیه؟',
    type: 'group',
    group: {
      items: [
        {
          key: RemindTime.Any,
          value: 'فرقی نداره',
        },
        {
          key: RemindTime.Morning,
          value: 'صبح',
        },
        {
          key: RemindTime.Afternoon,
          value: 'ظهر',
        },
        {
          key: RemindTime.Night,
          value: 'شب',
        },
      ],
    },
  },
];

// const thirdForm = (
//   state: State,
//   dispatch: React.Dispatch<Partial<State>>,
// ): FormItem[] => [

// ];

// const validateRepeat = (v: string): string => {
//   if (v.match(/[2-7]/)) {
//     return v;
//   }
//   return '';
// };
