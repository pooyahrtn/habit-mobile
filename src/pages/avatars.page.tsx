import React from 'react';
import { useAvatars } from '@controller/useAvatars';
import { AvatarView } from '@components/avatars.view';
import { useSetupProfile } from '@controller/profile/useSetupProfile';
import { useMe } from '@controller/profile/useMe';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';
import * as R from 'ramda';

export const AvatarPage = ({
  navigation,
}: PageProps<MainStackParams, 'avatars'>) => {
  const { avatars } = useAvatars();
  const { updateMe } = useSetupProfile();
  const { me } = useMe();
  const onItemSelect = (url: string) => {
    const image = url.substring(
      url.lastIndexOf('/') + 1,
      url.lastIndexOf('.png'),
    );
    console.log(image);
    if (image && me && me.profile && me.settings) {
      // TODO: fuck
      updateMe({
        profile: {
          ...R.omit(['__typename'], me.profile),
          image,
        },
        settings: {
          ...R.omit(['__typename'], me.settings),
        },
      });
      navigation.goBack();
    }
  };
  return (
    <AvatarView
      items={avatars.map((a) => ({
        image: a,
        onSelect: onItemSelect,
      }))}
    />
  );
};
