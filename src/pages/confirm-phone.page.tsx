import React, { useState, useCallback, useEffect } from 'react';
import { ConfirmView } from '@components/confirm-phone/confirm-phone.view';
import { usePhoneRegister } from '../controller/login/usePhoneRegister';
import { EntryStackParams } from '../route/params';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { toPersian } from '@utils/text';
import { useConfirmPhone } from '../controller/login/useConfirmPhone';

interface ConfirmPageProps {
  navigation: StackNavigationProp<EntryStackParams, 'confirmPhone'>;
  route: RouteProp<EntryStackParams, 'confirmPhone'>;
}
export const ConfirmPage = (props: ConfirmPageProps) => {
  const {
    route: {
      params: { phoneNumber },
    },
    navigation,
  } = props;
  const { registerPhone, registerErrors, showNextTry } = usePhoneRegister();
  const { requestConfirm, confirmErrors } = useConfirmPhone();
  useEffect(() => {
    registerPhone(phoneNumber);
  }, [registerPhone, phoneNumber]);

  const [confirmCode, setConfirmCode] = useState<string>('');
  const onConfirmCodeChange = useCallback(
    (c: string) => {
      setConfirmCode(c);
      if (c.length === 5) {
        requestConfirm(phoneNumber, c);
      }
    },
    [requestConfirm, phoneNumber],
  );

  return (
    <ConfirmView
      phoneNumber={toPersian(phoneNumber)}
      confirmCode={confirmCode}
      onConfirmCodeChange={onConfirmCodeChange}
      error={
        registerErrors && registerErrors.length > 0
          ? registerErrors[0].message
          : confirmErrors && confirmErrors.length > 0
          ? confirmErrors[0].message
          : undefined
      }
      onChangePhonePressed={() => navigation.goBack()}
      showResend={showNextTry}
      onResendPressed={() => registerPhone(phoneNumber)}
    />
  );
};
