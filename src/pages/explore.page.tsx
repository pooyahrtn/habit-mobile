import React from 'react';
import { ExploreView } from '@components/explore/explore.view';
import { useGetChallenges } from '@controller/challenge/useGetChallenges';
import {
  Challenges,
  Challenges_challenges,
} from '@graphql/queries/__generated__/Challenges';
import { ChallengeListItemProps } from '@components/challenge/challenge-list-item';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';
import { useFocusEffect } from '@react-navigation/native';
import { useCanAcceptMore } from '@controller/challenge/useCanAcceptMore';
import { useMe } from '@controller/profile/useMe';
import analytics from '@react-native-firebase/analytics';

export const ExplorePage = (props: PageProps<MainStackParams, 'tabs'>) => {
  const { navigation } = props;
  const { data, loading, refetch } = useGetChallenges();
  const { canAcceptMore } = useCanAcceptMore();
  const { me } = useMe();
  useFocusEffect(() => {
    refetch();
  });

  const onChallengePressed = (c: Challenges_challenges) => {
    analytics().logEvent('explore_challenge_pressed');
    navigation.navigate('challengeDeta', { challenge: c, mine: false });
  };

  const onOpenChallengePressed = () => {
    navigation.navigate('newChallenge');
  };

  const items = data ? challengeToChallengeProps(data, onChallengePressed) : [];

  return (
    <ExploreView
      items={items}
      loading={loading}
      showCannotAcceptMore={!canAcceptMore}
      showAddButton={(me?.game?.level ?? 0) > 1}
      onAddButtonPressed={onOpenChallengePressed}
    />
  );
};

const challengeToChallengeProps = (
  challenges: Challenges,
  onPressed: (c: Challenges_challenges) => void,
): ChallengeListItemProps[] => {
  return challenges.challenges.map((c) => ({
    id: c.id,
    imageURI: c.image,
    name: c.name,
    goal: c.goal,
    toughness: c.toughness,
    onPress: () => onPressed(c),
  }));
};
