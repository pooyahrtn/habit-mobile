import React, { useState, createContext, useContext, useCallback } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import DoneImage from '../assets/images/challenge/challenge-done.svg';
import { Text, Button } from '../components/common';
import { gStyle } from '../components/styles';
import { View as AnimatableView } from 'react-native-animatable';
import * as locale from '../locale';
import { useAcceptChallenge } from '@controller/challenge/useAcceptChallenge';
import { useToast } from '@components/Toast';

interface TaskCompleteModalConfig {
  challengeName: string;
  reward: number;
  challengeId: string;
}

type TaskCompleteCallback = (config: TaskCompleteModalConfig) => void;

const TaskCompleteContext = createContext<TaskCompleteCallback>(() => null);

export const TaskCompleteModal = (props: any) => {
  const [showModal, setShowModal] = useState(false);
  const [transition, setTransition] = useState<'enter' | 'exit'>('enter');
  const [config, setConfig] = useState<TaskCompleteModalConfig>();
  const { requestAcceptChallenge, loading } = useAcceptChallenge();
  const { showToast } = useToast();

  const requestShowModal = useCallback((c: TaskCompleteModalConfig) => {
    setTransition('enter');
    setShowModal(true);
    setConfig(c);
    console.log(c);
  }, []);

  const onConfirmRepeat = useCallback(async () => {
    if (!config) {
      return;
    }
    requestAcceptChallenge(config.challengeId);
    onClosePressed();
    showToast({
      message: locale.challenges.challengeAdded(config.challengeName),
      variation: 'success',
      duration: 1500,
    });
  }, [requestAcceptChallenge, config, showToast]);

  const onClosePressed = () => {
    setTransition('exit');
  };
  return (
    <TaskCompleteContext.Provider value={requestShowModal}>
      {props.children}
      {showModal && (
        <AnimatableView
          style={styles.container}
          animation={transition === 'exit' ? 'fadeOut' : undefined}
          duration={200}
          useNativeDriver={true}
          onAnimationEnd={() => {
            if (transition === 'exit') {
              setShowModal(false);
            }
          }}>
          <AnimatableView
            style={styles.content}
            animation="bounceIn"
            useNativeDriver={true}>
            <View style={styles.imageContainer}>
              <DoneImage width="100%" height="100%" />
            </View>
            <Text variation="h2" style={styles.text}>
              تبریک!
            </Text>
            <Text variation="b1" style={styles.text}>
              {locale.challenges.challengeDone(
                config?.challengeName ?? '',
                config?.reward ?? 0,
              )}
            </Text>
            <Text variation="b1" style={styles.text}>
              {locale.challenges.repeatChallenge}
            </Text>
            <View style={styles.buttonContainers}>
              <Button
                style={styles.noButton}
                content={{
                  text: 'اتمام چالش',
                  textStyle: { color: 'black' },
                }}
                onPress={onClosePressed}
                backgroundColor="background"
              />
              <Button
                style={styles.yesButton}
                content={{ text: 'تمدید چالش' }}
                onPress={onConfirmRepeat}
                loading={loading}
              />
            </View>
          </AnimatableView>
        </AnimatableView>
      )}
    </TaskCompleteContext.Provider>
  );
};

export const useTaskCompleteModal = () => {
  const showCompleteModal = useContext(TaskCompleteContext);
  return { showCompleteModal };
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.2)',
    justifyContent: 'center',
  },
  content: {
    backgroundColor: gStyle.colors.background,
    borderRadius: gStyle.layouts.l,
    padding: gStyle.layouts.l,
    margin: gStyle.layouts.l,
    ...gStyle.shadows.small,
    ...gStyle.shadows.center,
  },
  imageContainer: {
    width: '60%',
    alignSelf: 'center',
    aspectRatio: 1,
  },
  text: {
    marginBottom: gStyle.layouts.m,
  },
  buttonContainers: {
    flexDirection: 'row',
    marginTop: gStyle.layouts.l,
  },
  yesButton: {
    flex: 1,
  },
  noButton: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: gStyle.colors.background,
  },
});
