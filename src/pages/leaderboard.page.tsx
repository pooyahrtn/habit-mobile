import React, { useMemo } from 'react';
import { LeaderboardView } from '@components/leaderboard/leaderboard.view';
import { useLeaderboard } from '@controller/useLeaderboard';
import { Leaderboard_leaderboard } from '@graphql/queries/__generated__/Leaderboard';
import { LeaderboardItemProps } from '@components/leaderboard/leaderboard-item';

export const LeaderboardPage = () => {
  const { leaderboard } = useLeaderboard();
  const items = useMemo(() => transformLeaderboard(leaderboard), [leaderboard]);
  return <LeaderboardView items={items} />;
};

const transformLeaderboard = (
  items: Leaderboard_leaderboard[],
): LeaderboardItemProps[] => {
  return items.map((l) => ({
    username: l.profile.username,
    image: l.profile.image,
    score: l.game.score,
    id: l.id,
  }));
};
