import React from 'react';
import { AcceptChallengeView } from '@components/accept-challenge/accept-challenge.view';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';
import { useToast } from '@components/Toast';
import * as locale from '../locale';
import { useAcceptChallenge } from '@controller/challenge/useAcceptChallenge';
import analytics from '@react-native-firebase/analytics';

export const AcceptChallengePage = ({
  navigation,
  route,
}: PageProps<MainStackParams, 'acceptChallenge'>) => {
  const {
    params: { challenge },
  } = route;
  const { goBack } = navigation;

  const { showToast } = useToast();

  const { requestAcceptChallenge } = useAcceptChallenge();

  return (
    <AcceptChallengeView
      onClosePressed={goBack}
      challengeName={challenge.name}
      onConfirmed={() => {
        requestAcceptChallenge(challenge.id).then(() => {
          showToast({
            message: locale.challenges.challengeAdded(challenge.name),
            variation: 'success',
          });
        });
        navigation.popToTop();
      }}
    />
  );
};
