import React, { useState, useCallback } from 'react';
import { SetupProfile } from '@components/setup-profile/setup-profile.view';
import { Sex } from '@graphql/types/graphql-global-types';
import { useSetupProfile } from '@controller/profile/useSetupProfile';
import { toEnglish } from '@utils/text';

export const SetupPage = () => {
  const [username, setUsername] = useState('');
  const [age, setAge] = useState('');
  const [sex, setSex] = useState<Sex>(Sex.Other);
  const { requestSetupUser, loading } = useSetupProfile({ firstSetup: true });

  const onConfirmPressed = useCallback(() => {
    requestSetupUser({
      profile: {
        username,
        age: +toEnglish(age),
        sex,
      },
      settings: {
        notifications: true,
      },
    });
  }, [requestSetupUser, age, sex, username]);
  return (
    <SetupProfile
      age={age}
      onAgeChange={setAge}
      selectedSex={sex}
      onSexChange={setSex}
      username={username}
      onUsernameChange={setUsername}
      disabled={username.length === 0 || age.length === 0}
      onConfirmPressed={onConfirmPressed}
      loadingSetProfile={loading}
    />
  );
};
