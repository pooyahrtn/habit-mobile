import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

export interface PageProps<
  T extends Record<string, object | undefined>,
  K extends keyof T
> {
  navigation: StackNavigationProp<T, K>;
  route: RouteProp<T, K>;
}
