import React from 'react';
import { SafeArea, Text } from '@components/common';
import { StyleSheet, View } from 'react-native';
import { gStyle } from '@components/styles';
import LoginImage from '../assets/images/login/login.svg';

export const ForceUpdatePage = () => {
  return (
    <SafeArea style={styles.container}>
      <View style={styles.imageContainer}>
        <LoginImage width="100%" height="100%" />
        <Text variation="h1" style={styles.texts}>
          آپدیت!
        </Text>
        <Text variation="b1" style={styles.texts}>
          متاسفانه این نسخه دیگر پشتیبانی نمیشود. لطفا برای استفاده اپلیکیشن را
          آپدیت کنید
        </Text>
      </View>
    </SafeArea>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: gStyle.layouts.l,
    alignItems: 'center',
  },
  imageContainer: {
    width: '60%',
    aspectRatio: 1,
    margin: gStyle.layouts.xl,
  },
  texts: {
    textAlign: 'center',
    margin: gStyle.layouts.l,
  },
});
