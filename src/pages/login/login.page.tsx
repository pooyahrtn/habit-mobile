import React, { useState, useCallback } from 'react';
import { LoginView } from '@components/login/login.view';
import { StackNavigationProp } from '@react-navigation/stack';
import { EntryStackParams } from '@route/params';
import { Linking } from 'react-native';

interface LoginPageProps {
  navigation: StackNavigationProp<EntryStackParams>;
}

export const LoginPage = (props: LoginPageProps) => {
  const { navigation } = props;
  const [phoneNumber, setPhoneNumber] = useState<string>();
  const onPrivacyPressed = () => Linking.openURL('https://pupaaap.ir/privacy');

  const onPhoneChange = useCallback(
    (phone: string) => {
      setPhoneNumber(phone);
      if (phone.length === 11) {
        navigation.navigate('confirmPhone', { phoneNumber: phone });
      }
    },
    [navigation],
  );
  return (
    <LoginView
      phoneNumber={phoneNumber}
      onPhoneChange={onPhoneChange}
      onPrivacyPressed={onPrivacyPressed}
    />
  );
};
