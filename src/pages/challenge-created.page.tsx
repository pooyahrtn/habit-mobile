import React from 'react';
import CreatedImage from '../assets/images/challenge/created.svg';
import { StyleSheet, View } from 'react-native';
import { gStyle } from '@components/styles';
import { SafeArea, Text, Button } from '@components/common';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';

export const ChallengeCreatedView = ({
  navigation,
}: PageProps<MainStackParams, 'challengeCreated'>) => {
  const onDonePressed = () => {
    navigation.popToTop();
  };
  return (
    <SafeArea style={styles.display}>
      <View style={styles.imageContainer}>
        <CreatedImage />
      </View>
      <Text variation="h1">مرسی!</Text>
      <Text variation="b1" style={styles.congratsTexts}>
        چالش شما برای بررسی ارسال شد. در صورت تایید، چالش با نام شما نشان داده
        خواهد شد، و متناسب با بازخورد امتیاز به شما تعلق خواهد گرفت.
      </Text>
      <View style={styles.escapeView} />
      <Button
        content={{ text: 'خب', iconEnd: 'md-checkmark' }}
        onPress={onDonePressed}
      />
    </SafeArea>
  );
};

const styles = StyleSheet.create({
  display: {
    padding: gStyle.layouts.l,
    alignItems: 'center',
  },
  imageContainer: {
    width: '60%',
    aspectRatio: 1,
  },
  escapeView: {
    flex: 1,
  },
  congratsTexts: {
    marginVertical: gStyle.layouts.l,
  },
});
