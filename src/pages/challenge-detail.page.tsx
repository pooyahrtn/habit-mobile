import React, { useState } from 'react';
import { ChallengeDetailView } from '@components/challenge-detail/challenge-detail.view';
import { MainStackParams } from '@route/params';
import { PageProps } from './utils/PageProps';
import { useCanceChallenge } from '@controller/challenge/useCancelChallenge';
import { useCanAcceptMore } from '@controller/challenge/useCanAcceptMore';
import { ReportModalView } from '@components/report-modal.view';
import { useReportChallenge } from '@controller/challenge/useReportChallenge';
import { useToast } from '@components/Toast';

export const ChallengeDetailPage = (
  props: PageProps<MainStackParams, 'challengeDeta'>,
) => {
  const {
    route: {
      params: { challenge, mine },
    },
    navigation: { navigate, goBack, popToTop },
  } = props;
  const { canAcceptMore } = useCanAcceptMore();
  const [showMoreModal, setShowMoreModal] = useState(false);
  const { cancelChallenge, loading } = useCanceChallenge();
  const { report } = useReportChallenge();
  const { showToast } = useToast();
  return (
    <>
      <ChallengeDetailView
        challenge={challenge}
        onBackPressed={() => goBack()}
        variation={mine ? 'mine' : !canAcceptMore ? 'show' : 'new'}
        onConfirmPressed={() => navigate('acceptChallenge', { challenge })}
        loading={loading}
        onMorePressed={() => setShowMoreModal(true)}
        onDeletePressed={() => {
          console.log(challenge.id);
          cancelChallenge(challenge.id).then(popToTop);
        }}
      />
      <ReportModalView
        visible={showMoreModal}
        hideModal={() => setShowMoreModal(false)}
        onReportPressed={() => {
          report(challenge.id).then(() => {
            showToast({ variation: 'success', message: 'گزارش شما ثبت شد' });
          });
          setShowMoreModal(false);
        }}
      />
    </>
  );
};
