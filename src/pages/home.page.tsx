import React, { useState, useMemo, useCallback } from 'react';
import { HomeView, HomeTab, TaskItemProps } from '@components/home/home.view';
import { useMe } from '@controller/profile/useMe';
import { useToturial } from '@controller/useTutorial';
import { useMySchedule } from '@controller/tasks/useMySchedule';
import { useDoTask } from '@controller/tasks/useDoTask';
import { useFocusEffect } from '@react-navigation/native';
import { homeTasksStatus } from '@graphql/fragments/__generated__/homeTasksStatus';
import { PageProps } from './utils/PageProps';
import { MainStackParams } from '@route/params';
import analytics from '@react-native-firebase/analytics';

export const HomePage = ({
  navigation,
}: PageProps<MainStackParams, 'tabs'>) => {
  const { me } = useMe();
  const { tutorial, setTutorial } = useToturial();
  const { tasks, refreshIfneeded } = useMySchedule();
  const { doTask } = useDoTask();
  const [selectedTab, setSelectedTab] = useState<HomeTab>('today');
  const onItemDone = doTask;
  const onItemOpen = useCallback(
    (c: any) => {
      analytics().logEvent('home_open_item');
      navigation.navigate('challengeDeta', { challenge: c, mine: true });
    },
    [navigation],
  );

  useFocusEffect(() => {
    refreshIfneeded();
  });

  const homeTasks = useMemo(() => {
    return tasks?.map(transformedTask(onItemDone));
  }, [tasks, onItemDone]);

  const homeStatuses = useMemo(() => {
    return tasks?.map(transformStatus(onItemOpen));
  }, [tasks, onItemOpen]);

  const onSettingPressed = useCallback(() => {
    analytics().logEvent('home_settings_pressed');
    navigation.navigate('settings');
  }, [navigation]);

  return (
    <HomeView
      header={{
        level: me?.game?.level ?? 0,
        progress: me?.game?.progress ?? 0,
        score: me?.game?.score ?? 0,
        image: me?.profile?.image ?? '',
        onSettingPressed,
      }}
      selectedTab={selectedTab}
      onTabSelect={setSelectedTab}
      homeTaskProps={{
        hideTutorial: tutorial?.tutorial?.seenTasks ?? true,
        onTutorialRead: () => setTutorial('seenTasks'),
        tasks: homeTasks ?? [],
      }}
      homeStatusProps={{
        hideTutorial: tutorial?.tutorial?.seenTaskStatus ?? true,
        onTutorialRead: () => setTutorial('seenTaskStatus'),
        homeStatusItems: homeStatuses ?? [],
      }}
    />
  );
};

const transformedTask = (onItemSelect: (id: string) => void) => (
  t: homeTasksStatus,
): TaskItemProps => ({
  remindText: t.challenge.remindText,
  reward: t.reward,
  done: t.isDone,
  id: t.taskId,
  onDone: onItemSelect,
  remindTime: t.challenge.remindTime,
});

const transformStatus = (onItemPress: (c: any) => void) => (
  t: homeTasksStatus,
) => ({
  id: t.taskId,
  goal: t.challenge.goal,
  title: t.challenge.name,
  repeatCount: t.challenge.repeatCount,
  nCompleted: t.nCompleted,
  onPress: () => onItemPress(t.challenge),
});
