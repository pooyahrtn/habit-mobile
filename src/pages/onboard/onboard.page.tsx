import React, { createRef, useCallback, useState } from 'react';
import { OnboardView } from '../../components/onbard';
import { onboardItems } from './onboard.data';
import { useApolloClient } from '@apollo/react-hooks';
import { LocalData } from '../../graphql/localData';
import { AppEnterState } from '../../graphql/types/graphql-global-types';

export default () => {
  const carousalRef = createRef<any>();
  const [activeSlide, setActiveSlide] = useState(0);
  const apolloClient = useApolloClient();

  const onNextButtonPressed = useCallback(() => {
    carousalRef.current.snapToNext();
    if (activeSlide === onboardItems.length - 1) {
      apolloClient.writeData<LocalData>({
        data: {
          appEnterState: AppEnterState.WAIT_FOR_LOGIN,
        },
      });
    }
  }, [activeSlide, carousalRef, apolloClient]);

  const onActiveSlideChange = useCallback((index: number) => {
    setActiveSlide(index);
  }, []);

  return (
    <OnboardView
      items={onboardItems}
      onSnapToSlide={onActiveSlideChange}
      onNextButtonPressed={onNextButtonPressed}
      activeSlideNumber={activeSlide}
      ref={carousalRef}
    />
  );
};
