import React from 'react';
import { OnboardItemProps } from '../../components/onbard';
import * as locale from '../../locale';
import OnboardOneImage from '../../assets/images/onboard/onboard_one.svg';
import OnboardSecondImage from '../../assets/images/onboard/onboard_two.svg';
import ThirdImage from '../../assets/images/onboard/onboard_three.svg';

export const onboardItems: OnboardItemProps[] = [
  {
    key: '0',
    title: locale.onboard.slideOneTitle,
    body: locale.onboard.slideOneBody,
    image: <OnboardOneImage />,
  },
  {
    key: '1',
    title: locale.onboard.slideTwoTitle,
    body: locale.onboard.slideTwoBody,
    image: <OnboardSecondImage />,
  },
  {
    key: '2',
    title: locale.onboard.slideThreeTitle,
    body: locale.onboard.slideThreeBody,
    image: <ThirdImage width="100%" height="100%" />,
  },
];
