import React, { useReducer, useCallback } from 'react';
import { SettingView } from '@components/settings/settings.view';
import { useMe } from '@controller/profile/useMe';
import { Sex } from '@graphql/types/graphql-global-types';
import { MainStackParams } from '@route/params';
import { PageProps } from './utils/PageProps';
import { useSetupProfile } from '@controller/profile/useSetupProfile';

interface State {
  username: string;
  sex: Sex;
  age: string;
  notifications: boolean;
}

export const SettingPage = ({
  navigation,
}: PageProps<MainStackParams, 'settings'>) => {
  const { me } = useMe();
  const initState: State = {
    username: me?.profile?.username ?? '',
    sex: me?.profile?.sex ?? Sex.Other,
    age: me?.profile?.age + '' ?? '',
    notifications: me?.settings?.notifications ?? true,
  };
  const [state, dispatch] = useReducer(
    (prevState: State, newState: Partial<State>) => ({
      ...prevState,
      ...newState,
    }),
    initState,
  );

  const { updateMe } = useSetupProfile();

  const onSavePressed = useCallback(() => {
    const { sex, age, username, notifications } = state;

    updateMe({
      profile: {
        sex,
        age: +age,
        username,
      },
      settings: {
        notifications,
      },
    }).then(console.log);
    navigation.goBack();
  }, [updateMe, state, navigation]);

  const fieldSet = <K extends keyof State>(key: K) => (value: State[K]) =>
    dispatch({ [key]: value });

  return (
    <SettingView
      username={state.username}
      setUsername={fieldSet('username')}
      age={state.age}
      onAgeChange={fieldSet('age')}
      selectedSex={state.sex}
      onSexChange={fieldSet('sex')}
      image={me?.profile?.image ?? ''}
      notifications={state.notifications}
      setNotifications={fieldSet('notifications')}
      onClosePressed={navigation.goBack}
      onSavePressed={onSavePressed}
      onAvatarPressed={() => navigation.navigate('avatars')}
    />
  );
};
