import React, { useCallback } from 'react';
import { FirstChallengeView } from '@components/first-challenge/first-challenge.view';
import { useStartupChallenges } from '@controller/challenge/useStartupChallenges';
import { Challenges } from '@graphql/queries/__generated__/Challenges';
import { ChallengeListItemProps } from '@components/challenge/challenge-list-item';
import { useAcceptChallenge } from '@controller/challenge/useAcceptChallenge';
import { useAppState } from '@controller/startup/useAppState';
import { AppEnterState } from '@graphql/types/graphql-global-types';

export const FirstChallengePage = () => {
  const { loading, data } = useStartupChallenges();
  const { requestAcceptChallenge } = useAcceptChallenge();
  const { setAppState } = useAppState();
  const onChallengeSelect = useCallback(
    (id: string) => {
      requestAcceptChallenge(id);
      setAppState(AppEnterState.LOGGED_IN);
    },
    [requestAcceptChallenge, setAppState],
  );
  const items = data ? challengeToChallengeProps(data, onChallengeSelect) : [];

  const onSkipPressed = () => setAppState(AppEnterState.LOGGED_IN);

  return (
    <FirstChallengeView
      loading={loading}
      items={items}
      onSkipPressed={onSkipPressed}
    />
  );
};

const challengeToChallengeProps = (
  challenges: Challenges,
  onPressed: (id: string) => void,
): ChallengeListItemProps[] => {
  return challenges.challenges.map((c) => ({
    id: c.id,
    imageURI: c.image,
    name: c.name,
    goal: c.goal,
    toughness: c.toughness,
    onPress: () => onPressed(c.id),
  }));
};
