import gql from 'graphql-tag';

export default gql`
  mutation ConfirmPhone($input: VerifyPhoneInput!) {
    verifyPhone(input: $input) {
      accessToken
      refreshToken
    }
  }
`;
