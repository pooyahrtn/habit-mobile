import gql from 'graphql-tag';

export default gql`
  mutation CreateChallenge($input: CreateChallengeInput!) {
    createChallenge(challenge: $input) {
      id
      name
    }
  }
`;
