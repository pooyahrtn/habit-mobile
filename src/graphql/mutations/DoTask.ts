import gql from 'graphql-tag';

export default gql`
  mutation DoTask($input: DoTaskInput!) {
    doTask(doTaskInput: $input) {
      newLevel
      reward
      completed
      challengeName
      challengeId
    }
  }
`;
