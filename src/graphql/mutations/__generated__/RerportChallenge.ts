/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ReportChallengeInput } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: RerportChallenge
// ====================================================

export interface RerportChallenge {
  reportChallenge: boolean;
}

export interface RerportChallengeVariables {
  input: ReportChallengeInput;
}
