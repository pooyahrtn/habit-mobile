/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AcceptChallengeInput, Day, RemindTime } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: AcceptChallenge
// ====================================================

export interface AcceptChallenge_acceptChallenge_taskStatuses_challenge {
  __typename: "Challenge";
  name: string;
  goal: string;
  remindText: string;
  remindTime: RemindTime;
}

export interface AcceptChallenge_acceptChallenge_taskStatuses {
  __typename: "TaskStatus";
  taskId: string;
  nCompleted: number;
  reward: number;
  isDone: boolean;
  isCompleted: boolean;
  challenge: AcceptChallenge_acceptChallenge_taskStatuses_challenge;
}

export interface AcceptChallenge_acceptChallenge {
  __typename: "DayTask";
  day: Day;
  taskStatuses: AcceptChallenge_acceptChallenge_taskStatuses[];
}

export interface AcceptChallenge {
  acceptChallenge: AcceptChallenge_acceptChallenge[];
}

export interface AcceptChallengeVariables {
  input: AcceptChallengeInput;
}
