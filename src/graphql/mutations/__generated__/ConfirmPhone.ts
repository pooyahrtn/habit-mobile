/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { VerifyPhoneInput } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ConfirmPhone
// ====================================================

export interface ConfirmPhone_verifyPhone {
  __typename: "Auth";
  accessToken: string;
  refreshToken: string;
}

export interface ConfirmPhone {
  verifyPhone: ConfirmPhone_verifyPhone;
}

export interface ConfirmPhoneVariables {
  input: VerifyPhoneInput;
}
