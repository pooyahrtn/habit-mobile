/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DoTaskInput } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DoTask
// ====================================================

export interface DoTask_doTask {
  __typename: "DoTaskResponse";
  newLevel: boolean;
  reward: number;
  completed: boolean;
  challengeName: string;
  challengeId: string;
}

export interface DoTask {
  doTask: DoTask_doTask;
}

export interface DoTaskVariables {
  input: DoTaskInput;
}
