/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OpenAppInput } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: OpenApp
// ====================================================

export interface OpenApp_openApp {
  __typename: "OpenApp";
  androidMinVersion: number;
}

export interface OpenApp {
  openApp: OpenApp_openApp;
}

export interface OpenAppVariables {
  input: OpenAppInput;
}
