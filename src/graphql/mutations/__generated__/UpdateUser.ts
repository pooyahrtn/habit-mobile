/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateUserInput, Sex } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UpdateUser
// ====================================================

export interface UpdateUser_updateMe_profile {
  __typename: "Profile";
  username: string;
  age: number;
  sex: Sex;
  image: string;
}

export interface UpdateUser_updateMe_settings {
  __typename: "Settings";
  notifications: boolean;
}

export interface UpdateUser_updateMe_game {
  __typename: "Game";
  score: number;
  level: number;
  progress: number;
}

export interface UpdateUser_updateMe {
  __typename: "User";
  id: string;
  profile: UpdateUser_updateMe_profile | null;
  settings: UpdateUser_updateMe_settings | null;
  game: UpdateUser_updateMe_game | null;
}

export interface UpdateUser {
  updateMe: UpdateUser_updateMe;
}

export interface UpdateUserVariables {
  input: UpdateUserInput;
}
