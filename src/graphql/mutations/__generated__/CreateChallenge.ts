/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateChallengeInput } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CreateChallenge
// ====================================================

export interface CreateChallenge_createChallenge {
  __typename: "Challenge";
  id: string;
  name: string;
}

export interface CreateChallenge {
  createChallenge: CreateChallenge_createChallenge;
}

export interface CreateChallengeVariables {
  input: CreateChallengeInput;
}
