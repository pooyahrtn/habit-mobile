/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RemoveChallengeInput, Day, Toughness, RemindTime, ChallengeCategory } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CancelChallenge
// ====================================================

export interface CancelChallenge_cancelChallenge_taskStatuses_challenge_stats {
  __typename: "ChallengeStats";
  successCount: number;
  enrolledCount: number;
}

export interface CancelChallenge_cancelChallenge_taskStatuses_challenge {
  __typename: "Challenge";
  id: string;
  name: string;
  toughness: Toughness;
  stats: CancelChallenge_cancelChallenge_taskStatuses_challenge_stats;
  remindText: string;
  repeatCount: number;
  remindTime: RemindTime;
  goal: string;
  description: string;
  category: ChallengeCategory;
  image: string;
}

export interface CancelChallenge_cancelChallenge_taskStatuses {
  __typename: "TaskStatus";
  id: string;
  taskId: string;
  nCompleted: number;
  reward: number;
  isDone: boolean;
  isCompleted: boolean;
  challenge: CancelChallenge_cancelChallenge_taskStatuses_challenge;
}

export interface CancelChallenge_cancelChallenge {
  __typename: "DayTask";
  day: Day;
  taskStatuses: CancelChallenge_cancelChallenge_taskStatuses[];
}

export interface CancelChallenge {
  cancelChallenge: CancelChallenge_cancelChallenge[];
}

export interface CancelChallengeVariables {
  input: RemoveChallengeInput;
}
