import gql from 'graphql-tag';

export default gql`
  mutation AcceptChallenge($input: AcceptChallengeInput!) {
    acceptChallenge(acceptChallengeInput: $input) {
      day
      taskStatuses {
        taskId
        nCompleted
        reward
        isDone
        reward
        isCompleted
        challenge {
          name
          goal
          remindText
          remindTime
        }
      }
    }
  }
`;
