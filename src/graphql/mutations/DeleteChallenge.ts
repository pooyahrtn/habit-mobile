import gql from 'graphql-tag';
import HomeTaskStatus from '../fragments/HomeTask';

export default gql`
  mutation CancelChallenge($input: RemoveChallengeInput!) {
    cancelChallenge(cancelInput: $input) {
      day
      taskStatuses {
        ...homeTasksStatus
      }
    }
  }
  ${HomeTaskStatus}
`;
