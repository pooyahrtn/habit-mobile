import gql from 'graphql-tag';

export default gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateMe(user: $input) {
      id
      profile {
        username
        age
        sex
        image
      }
      settings {
        notifications
      }
      game {
        score
        level
        progress
      }
    }
  }
`;
