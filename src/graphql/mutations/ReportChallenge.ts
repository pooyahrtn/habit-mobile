import gql from 'graphql-tag';

export default gql`
  mutation RerportChallenge($input: ReportChallengeInput!) {
    reportChallenge(input: $input)
  }
`;
