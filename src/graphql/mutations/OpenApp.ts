import gql from 'graphql-tag';
export default gql`
  mutation OpenApp($input: OpenAppInput!) {
    openApp(input: $input) {
      androidMinVersion
    }
  }
`;
