import { AppEnterState } from './types/graphql-global-types';
import { Tutorial_tutorial } from './queries/__generated__/Tutorial';

export interface LocalData {
  appEnterState: AppEnterState;
  tutorial: Tutorial_tutorial;
}

export const initData: LocalData = {
  appEnterState: AppEnterState.JUST_ENTER,
  tutorial: {
    __typename: 'Tutorial',
    seenTasks: false,
    seenTaskStatus: false,
  },
};
