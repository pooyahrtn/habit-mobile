import gql from 'graphql-tag';

export default gql`
  query Me {
    me {
      id
      profile {
        username
        age
        sex
        image
      }
      settings {
        notifications
      }
      game {
        score
        level
        progress
      }
    }
  }
`;
