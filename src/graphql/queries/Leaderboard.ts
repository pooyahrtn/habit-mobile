import gql from 'graphql-tag';

export default gql`
  query Leaderboard {
    leaderboard {
      id
      profile {
        image
        username
      }
      game {
        score
      }
    }
  }
`;
