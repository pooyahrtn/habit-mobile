/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Day, Toughness, RemindTime, ChallengeCategory } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MySchedule
// ====================================================

export interface MySchedule_mySchedule_taskStatuses_challenge_stats {
  __typename: "ChallengeStats";
  successCount: number;
  enrolledCount: number;
}

export interface MySchedule_mySchedule_taskStatuses_challenge {
  __typename: "Challenge";
  id: string;
  name: string;
  toughness: Toughness;
  stats: MySchedule_mySchedule_taskStatuses_challenge_stats;
  remindText: string;
  repeatCount: number;
  remindTime: RemindTime;
  goal: string;
  description: string;
  category: ChallengeCategory;
  image: string;
}

export interface MySchedule_mySchedule_taskStatuses {
  __typename: "TaskStatus";
  id: string;
  taskId: string;
  nCompleted: number;
  reward: number;
  isDone: boolean;
  isCompleted: boolean;
  challenge: MySchedule_mySchedule_taskStatuses_challenge;
}

export interface MySchedule_mySchedule {
  __typename: "DayTask";
  day: Day;
  taskStatuses: MySchedule_mySchedule_taskStatuses[];
}

export interface MySchedule {
  mySchedule: MySchedule_mySchedule[];
}
