/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CanAcceptMoreChallenges
// ====================================================

export interface CanAcceptMoreChallenges {
  canAcceptMore: boolean;
}
