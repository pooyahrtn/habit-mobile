/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GetChallengeInput, Toughness, RemindTime, ChallengeCategory } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: Challenges
// ====================================================

export interface Challenges_challenges_stats {
  __typename: "ChallengeStats";
  successCount: number;
  enrolledCount: number;
}

export interface Challenges_challenges_user_profile {
  __typename: "Profile";
  username: string;
  image: string;
}

export interface Challenges_challenges_user {
  __typename: "User";
  id: string;
  profile: Challenges_challenges_user_profile | null;
}

export interface Challenges_challenges {
  __typename: "Challenge";
  id: string;
  name: string;
  toughness: Toughness;
  stats: Challenges_challenges_stats;
  remindText: string;
  repeatCount: number;
  remindTime: RemindTime;
  goal: string;
  description: string;
  category: ChallengeCategory;
  image: string;
  user: Challenges_challenges_user;
}

export interface Challenges {
  challenges: Challenges_challenges[];
}

export interface ChallengesVariables {
  filter: GetChallengeInput;
}
