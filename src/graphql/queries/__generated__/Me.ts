/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sex } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: Me
// ====================================================

export interface Me_me_profile {
  __typename: "Profile";
  username: string;
  age: number;
  sex: Sex;
  image: string;
}

export interface Me_me_settings {
  __typename: "Settings";
  notifications: boolean;
}

export interface Me_me_game {
  __typename: "Game";
  score: number;
  level: number;
  progress: number;
}

export interface Me_me {
  __typename: "User";
  id: string;
  profile: Me_me_profile | null;
  settings: Me_me_settings | null;
  game: Me_me_game | null;
}

export interface Me {
  me: Me_me;
}
