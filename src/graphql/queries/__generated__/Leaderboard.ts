/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Leaderboard
// ====================================================

export interface Leaderboard_leaderboard_profile {
  __typename: "Profile";
  image: string;
  username: string;
}

export interface Leaderboard_leaderboard_game {
  __typename: "Game";
  score: number;
}

export interface Leaderboard_leaderboard {
  __typename: "LeaderBoardUser";
  id: string;
  profile: Leaderboard_leaderboard_profile;
  game: Leaderboard_leaderboard_game;
}

export interface Leaderboard {
  leaderboard: Leaderboard_leaderboard[];
}
