/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AppEnterState } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: AppEnterStateQuery
// ====================================================

export interface AppEnterStateQuery {
  appEnterState: AppEnterState | null;
}
