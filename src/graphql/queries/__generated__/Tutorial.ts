/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Tutorial
// ====================================================

export interface Tutorial_tutorial {
  __typename: "Tutorial";
  seenTasks: boolean;
  seenTaskStatus: boolean;
}

export interface Tutorial {
  tutorial: Tutorial_tutorial;
}
