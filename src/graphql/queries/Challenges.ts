import gql from 'graphql-tag';

export default gql`
  query Challenges($filter: GetChallengeInput!) {
    challenges(filter: $filter) {
      id
      name
      toughness
      stats {
        successCount
        enrolledCount
      }
      remindText
      repeatCount
      remindTime
      goal
      description
      category
      image
      user {
        id
        profile {
          username
          image
        }
      }
    }
  }
`;
