import gql from 'graphql-tag';
import HomeTaskStatus from '../fragments/HomeTask';

export default gql`
  query MySchedule {
    mySchedule {
      day
      taskStatuses {
        ...homeTasksStatus
      }
    }
  }
  ${HomeTaskStatus}
`;
