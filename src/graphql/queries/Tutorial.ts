import gql from 'graphql-tag';

export default gql`
  query Tutorial {
    tutorial @client {
      seenTasks
      seenTaskStatus
    }
  }
`;
