import gql from 'graphql-tag';

export default gql`
  query AppEnterStateQuery {
    appEnterState @client
  }
`;
