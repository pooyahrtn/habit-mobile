import gql from 'graphql-tag';

export default gql`
  query AvatarImages {
    avatarImages
  }
`;
