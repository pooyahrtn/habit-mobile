import gql from 'graphql-tag';

export const typeDefs = gql`
  enum AppEnterState {
    JUST_ENTER
    WAIT_FOR_LOGIN
    WAIT_FOR_PROFILE
    PICK_FIRST_CHALLENGE
    LOGGED_IN
    FORCE_UPDATE
  }

  type Tutorial {
    seenTasks: Boolean!
    seenTaskStatus: Boolean!
  }

  extend type Query {
    appEnterState: AppEnterState
    tutorial: Tutorial!
  }
`;
