/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Toughness, RemindTime, ChallengeCategory } from "./../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: homeTasksStatus
// ====================================================

export interface homeTasksStatus_challenge_stats {
  __typename: "ChallengeStats";
  successCount: number;
  enrolledCount: number;
}

export interface homeTasksStatus_challenge {
  __typename: "Challenge";
  id: string;
  name: string;
  toughness: Toughness;
  stats: homeTasksStatus_challenge_stats;
  remindText: string;
  repeatCount: number;
  remindTime: RemindTime;
  goal: string;
  description: string;
  category: ChallengeCategory;
  image: string;
}

export interface homeTasksStatus {
  __typename: "TaskStatus";
  id: string;
  taskId: string;
  nCompleted: number;
  reward: number;
  isDone: boolean;
  isCompleted: boolean;
  challenge: homeTasksStatus_challenge;
}
