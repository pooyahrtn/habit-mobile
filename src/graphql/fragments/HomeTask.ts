import gql from 'graphql-tag';

export default gql`
  fragment homeTasksStatus on TaskStatus {
    id
    taskId
    nCompleted
    reward
    isDone
    reward
    isCompleted
    challenge {
      id
      name
      toughness
      stats {
        successCount
        enrolledCount
      }
      remindText
      repeatCount
      remindTime
      goal
      description
      category
      image
    }
  }
`;
