/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum AppEnterState {
  FORCE_UPDATE = "FORCE_UPDATE",
  JUST_ENTER = "JUST_ENTER",
  LOGGED_IN = "LOGGED_IN",
  PICK_FIRST_CHALLENGE = "PICK_FIRST_CHALLENGE",
  WAIT_FOR_LOGIN = "WAIT_FOR_LOGIN",
  WAIT_FOR_PROFILE = "WAIT_FOR_PROFILE",
}

export enum ChallengeCategory {
  Art = "Art",
  Beauty = "Beauty",
  Book = "Book",
  Business = "Business",
  Charity = "Charity",
  Entertainment = "Entertainment",
  Family = "Family",
  Food = "Food",
  Game = "Game",
  Health = "Health",
  Home = "Home",
  Learn = "Learn",
  Lifestyle = "Lifestyle",
  Music = "Music",
  Shopping = "Shopping",
  Social = "Social",
  Sport = "Sport",
  Travel = "Travel",
}

export enum Day {
  Friday = "Friday",
  Monday = "Monday",
  Saturday = "Saturday",
  Sunday = "Sunday",
  Thursday = "Thursday",
  Tuesday = "Tuesday",
  Wednesday = "Wednesday",
}

export enum RemindTime {
  Afternoon = "Afternoon",
  Any = "Any",
  Evening = "Evening",
  Morning = "Morning",
  Night = "Night",
}

export enum Sex {
  Female = "Female",
  Male = "Male",
  Other = "Other",
}

export enum Toughness {
  Easy = "Easy",
  Hard = "Hard",
  Medium = "Medium",
}

export interface AcceptChallengeInput {
  challengeId: string;
  days?: (Day | null)[] | null;
}

export interface CreateChallengeInput {
  name: string;
  goal: string;
  description: string;
  repeatCount: number;
  toughness: Toughness;
  remindTime: RemindTime;
  remindText: string;
  category: ChallengeCategory;
}

export interface CreateUserInput {
  phoneNumber: string;
}

export interface DoTaskInput {
  taskId: string;
  day: Day;
}

export interface GetChallengeInput {
  toughness?: Toughness | null;
  count?: number | null;
  trending?: boolean | null;
  firstChallenges?: boolean | null;
}

export interface OpenAppInput {
  fcmToken: string;
}

export interface ProfileInput {
  username: string;
  sex: Sex;
  age: number;
  image?: string | null;
}

export interface RemoveChallengeInput {
  challengeId: string;
}

export interface ReportChallengeInput {
  challengeId: string;
}

export interface SettingsInput {
  notifications: boolean;
}

export interface UpdateUserInput {
  profile: ProfileInput;
  settings: SettingsInput;
}

export interface VerifyPhoneInput {
  phoneNumber: string;
  confirmCode: string;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
