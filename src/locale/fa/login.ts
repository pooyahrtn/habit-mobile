export const login = {
  explanation:
    'برای از دست نرفتن اطلاعاتت، و سالم و امن نگه داشتن اپلیکشن،  نیاز به ثبت نام هست.',
  title: 'ثبت نام',
  withPhone: 'با شماره تماس',
  privacyPolicy: 'قوانین، شرایط وحریم خصوصی',
  confirmSent: (a: string) => `کد تایید، به شماره‌ی ${a} ارسال شد.`,
  enterPhonePlaceHolder: 'شماره‌ی تلفنت رو وارد کن',
  confirmCodePlaceHolder: 'کد تایید رو وارد کن',
  changePhone: ' اصلاح شماره تماس',
  resendCode: 'ارسال مجدد',
  confirmCode: 'کد تایید',
};
