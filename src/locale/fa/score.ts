import { toPersian } from '@utils/text';

export const score = {
  withScore: (s: number) => `${toPersian(s + '')} امتیاز`,
  withLevel: (s: number) => `لول ${toPersian('' + s)}`,
  newScore: (s: number) => `${toPersian(s + '')} امتیاز!`,
};
