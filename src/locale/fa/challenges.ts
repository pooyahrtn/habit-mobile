import { toPersian } from '@utils/text';

export const challenges = {
  challengesTitle: 'چالش ها',
  countEnroll: (n: number) =>
    `${toPersian(n + '')} نفر در این چالش شرکت کرده اند.`,
  countSuccess: (n: number) =>
    `${toPersian(n + '')} نفر این چالش را با موفقیت تمام کرده اند.`,
  goal: 'هدف',
  repeat: 'تکرار',
  perWeek: (n: number) => `${toPersian(n + '')} بار در هفته`,
  contract: 'قرارداد',
  promise: (name: string) =>
    `من قول میدم که چالش ${toPersian(name)} رو انجام بدم`,
  pressToStart: 'دکمه‌ی زیر رو نگه دار تا شروع کنی',
  challengeAdded: (n: string) => `چالش ${n} اضافه شد.`,
  challengeDone: (challengeName: string, reward: number) =>
    `چالش ${challengeName} رو با موفقیت تموم کردی و ${toPersian(
      reward + '',
    )} امتیاز گرفتی`,
  repeatChallenge: 'دوست داری این چالش رو بازم ادامه بدی؟',
  cannotAcceptMore: 'برای انتخاب چالش های بیشتر، چالش های فعلیت رو تموم کن',
  createNewChallengeDescription: `اگه چالشی رو میخوای که پیدا نکردی، اینجا میتونی چالش جدید بسازی.
چالش ها، کارهای کوچکی هستند، که به مدت یک هفته باید انجام شوند. 
چالش ها برای تیم ما ارسال میشه، و در صورت تایید، با نام شما برای همه نشون داده میشن.
هرچقدر که آدم های بیشتری چالشت رو انجام بدن، امتیازت بالاتر میره.  `,
};
