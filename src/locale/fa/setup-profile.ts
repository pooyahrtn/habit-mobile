export const setupProfile = {
  title: 'درباه‌ی خودت بگو',
  headerBody:
    'برای اینکه بتونیم پیشنهاد های بهتری بهت بدیم، میتونی در مورد خودت و اهدافت بمون بگی.',
  username: 'نام کاربری',
  usernameDetail: 'این اسمیه که به بقیه نشون داده میشه.',
  usernameHint:
    'یک اسم قشنگ انتخاب کن که اگه جزو نفرات برتر بودی بقیه بشناسنت. میتونه فارسی یا انگلیسی باشه.',
  usernamePlaceholder: 'نام کاربریت رو وارد کن',
  agePlaceholder: 'سنت رو وارد کن',
  age: 'سن',
  year: 'سال',
  sex: 'جنسیت',
  other: 'غیره',
  male: 'مرد',
  female: 'زن',
  confirm: 'تایید',
  notification: 'اعلانات',
  notificationDetil: 'وضعیت اعلانات: یادآوری ها، و پیام های جدید.',
};
