export const time = {
  any: 'هروقت تونستی',
  morning: 'صبح',
  afternoon: 'بعد ظهر',
  evening: 'عصر',
  night: 'شب',
};
