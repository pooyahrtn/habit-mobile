import AsyncStorage from '@react-native-community/async-storage';

const KEY = 'DATA_KEY';

interface Data {
  accessToken: string;
  refreshToken: string;
}

const initData: Data = {
  accessToken: '',
  refreshToken: '',
};

let data: Data;

export const restore = async () => {
  const serializedData = await AsyncStorage.getItem(KEY);
  if (serializedData) {
    data = JSON.parse(serializedData);
  } else {
    data = initData;
  }
};

export const setData = async (d: Partial<Data>) => {
  data = {
    ...data,
    ...d,
  };
  return await AsyncStorage.setItem(KEY, JSON.stringify(data));
};

export const getDatta = async () => {
  if (data) {
    return data;
  }
  await restore();
  return data;
};
