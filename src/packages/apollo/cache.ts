import { InMemoryCache } from 'apollo-boost';

export const getCache = (initData: any) => {
  const cache = new InMemoryCache({});
  cache.writeData({ data: initData });
  return cache;
};
