import { NormalizedCacheObject, DocumentNode, from } from 'apollo-boost';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { RetryLink } from 'apollo-link-retry';
import { authFlowLink } from './authLink';
import { getCache } from './cache';
import { persistCache } from 'apollo-cache-persist';
import AsyncStorage from '@react-native-community/async-storage';

export type ApolloClientType = ApolloClient<NormalizedCacheObject>;

export interface ApolloClientConfig {
  url: string;
  localTypeDefs?: DocumentNode;
  initData: any;
}

export async function getClient(conf: ApolloClientConfig) {
  const { localTypeDefs, initData, url } = conf;
  const cache = await getCache(initData);
  const httpLink = new HttpLink({ uri: url });
  const client = new ApolloClient<NormalizedCacheObject>({
    cache,
    resolvers: {},
    typeDefs: localTypeDefs,
    link: from([authFlowLink(url), new RetryLink(), httpLink]),
  });
  await persistCache({
    cache,
    storage: AsyncStorage as any,
    debug: true,
    maxSize: 5048576,
  });
  client.onResetStore(() =>
    Promise.resolve(cache.writeData({ data: initData })),
  );

  return client;
}
