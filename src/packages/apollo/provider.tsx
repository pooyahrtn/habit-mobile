import React, { FC } from 'react';
import { ApolloClient, NormalizedCacheObject } from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

export const createProvider = (client: ApolloClient<NormalizedCacheObject>) => (
  Component: FC,
): FC => {
  return (props) => {
    return (
      <ApolloProvider client={client}>
        <Component {...props} />
      </ApolloProvider>
    );
  };
};
