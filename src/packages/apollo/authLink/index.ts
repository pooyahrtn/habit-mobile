import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { getDatta } from '../../localstorage';
import { refreshTokenAndSave, AUTH_ERROR } from './refetchAuth';
import { Observable } from 'apollo-link';

type OnAuthFailedObserver = () => void;
let onAuthFailedObservers: OnAuthFailedObserver[] = [];

export function addOnAuthFailedObserver(obs: OnAuthFailedObserver) {
  onAuthFailedObservers = [...onAuthFailedObservers, obs];
}

const headersObject = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

const withToken = setContext(() => {
  // if you have a cached value, return it immediately

  return getDatta().then((db) => {
    if (db.accessToken) {
      return headersObject(db.accessToken);
    }
  });
});

function getSubscriber(observer: any) {
  return {
    next: observer.next.bind(observer),
    error: observer.error.bind(observer),
    complete: observer.complete.bind(observer),
  };
}

function graphqlErrorStatusCode(err: any) {
  if (
    err.extensions.exception &&
    err.extensions.exception &&
    err.extensions.exception.message &&
    err.extensions.exception.message.statusCode
  ) {
    return err.extensions.exception.message.statusCode;
  }
}

const resetToken = (url: string) =>
  onError(({ graphQLErrors, operation, forward, networkError }) => {
    if (networkError) {
      console.warn(`network: ${networkError}`);
    }
    if (graphQLErrors) {
      for (let err of graphQLErrors) {
        console.warn(err);
        const statusCode = graphqlErrorStatusCode(err);
        switch (statusCode) {
          case 401:
            return new Observable((observer) => {
              refreshTokenAndSave(url)
                .then(({ accessToken }) => {
                  operation.setContext(headersObject(accessToken));
                  const subscriber = getSubscriber(observer);
                  forward(operation).subscribe(subscriber);
                })
                .catch((e) => {
                  if (e.message === AUTH_ERROR) {
                    onAuthFailedObservers.forEach((obs) => obs());
                  }
                });
            });
        }
      }
    }
  });

export const authFlowLink = (url: string) => withToken.concat(resetToken(url));
