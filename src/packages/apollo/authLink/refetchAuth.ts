import { setData, getDatta } from '../../localstorage';

export const AUTH_ERROR = 'AUTH_ERROR';

type TokenResponse = { accessToken: string; refreshToken: string };

async function setTokens({ accessToken, refreshToken }: TokenResponse) {
  await setData({ accessToken, refreshToken });
  return { accessToken, refreshToken };
}

export async function refreshTokenAndSave(url: string) {
  return getDatta()
    .then((r) => r.refreshToken)
    .then(fetchAcessToken(url))
    .then(setTokens);
}

const fetchAcessToken = (url: string) => async (oldRefreshToken: string) => {
  const fetchResult = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: `
        mutation {
          refreshAccessToken(refreshToken: "${oldRefreshToken}"
          ) {
            accessToken
            refreshToken
          }
        }
      `,
    }),
  });

  const refreshResponse = await fetchResult.json();

  if (
    !refreshResponse ||
    !refreshResponse.data ||
    !refreshResponse.data.refreshAccessToken ||
    !refreshResponse.data.refreshAccessToken.accessToken ||
    !refreshResponse.data.refreshAccessToken.refreshToken
  ) {
    throw new Error(AUTH_ERROR);
  }

  const { accessToken, refreshToken } = refreshResponse.data.refreshAccessToken;
  return { accessToken, refreshToken };
};
