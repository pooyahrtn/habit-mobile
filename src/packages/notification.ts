import messaging from '@react-native-firebase/messaging';
import { useState } from 'react';

export async function registerNotifications() {
  await messaging().registerForRemoteNotifications();
  return messaging().getToken();
}

export const useNotifications = () => {
  const [loading, setLoading] = useState(false);
};
