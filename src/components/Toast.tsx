import React, {
  useState,
  useCallback,
  createContext,
  useContext,
  useEffect,
} from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { View } from 'react-native-animatable';
import { Text } from './common';
import { gStyle } from './styles';

interface ToastConfig {
  message: string;
  variation: 'success' | 'danger';
  duration?: number;
}
type ToastCallback = (config: ToastConfig) => void;

const ToastContext = createContext<ToastCallback>(() => null);

export const ToastWrapper = (props: any) => {
  const [showToast, setShowToast] = useState(false);
  const [config, setConfig] = useState<ToastConfig>();
  const [transition, setTransition] = useState<'show' | 'hide'>('show');

  useEffect(() => {
    let timeOut: any;
    if (transition === 'show' && showToast === true) {
      timeOut = setTimeout(() => {
        setTransition('hide');
      }, 1500);
    }
    return () => {
      clearTimeout(timeOut);
    };
  }, [transition, showToast]);

  const setToast = useCallback((c: ToastConfig) => {
    setShowToast(true);
    setTransition('show');
    setConfig(c);
  }, []);
  return (
    <ToastContext.Provider value={setToast}>
      {props.children}
      {showToast && (
        <View
          animation={transition === 'show' ? 'fadeIn' : 'fadeOut'}
          onAnimationEnd={() => {
            if (transition === 'hide') {
              setShowToast(false);
            }
          }}
          style={[
            styles.container,
            config?.variation === 'success' && styles.successContainer,
          ]}>
          <Text variation="b1" style={styles.text}>
            {config?.message}
          </Text>
        </View>
      )}
    </ToastContext.Provider>
  );
};

export const useToast = () => {
  const showToast = useContext(ToastContext);
  return { showToast };
};

const { width } = Dimensions.get('screen');
const margin = gStyle.layouts.l;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: width - 2 * margin,
    left: margin,
    bottom: 3 * margin,
    justifyContent: 'center',
    padding: gStyle.layouts.l,
    paddingVertical: gStyle.layouts.m,
    borderRadius: gStyle.layouts.l,
    ...gStyle.shadows.small,
    ...gStyle.shadows.center,
  },
  successContainer: {
    backgroundColor: '#3AC08E',
  },
  text: {
    color: 'white',
  },
});
