import React, { PropsWithChildren } from 'react';
import {
  Text as NativeText,
  TextProps as NativeTextProps,
  StyleSheet,
  TextStyle,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { gStyle, Color, TextVariations } from '../styles';

export interface TextProps extends NativeTextProps {
  variation: TextVariations;
  color?: Color;
}

export const Text = (props: PropsWithChildren<TextProps>) => {
  const { children, style, variation, color } = props;
  const Inside = (
    <NativeText
      style={[
        styles.base,
        styles.rtl,
        { color: color },
        variationStyles[variation],
        style,
      ]}>
      {children}
    </NativeText>
  );
  if (variation === 'link') {
    return (
      <TouchableOpacity onPress={props.onPress}>{Inside}</TouchableOpacity>
    );
  }
  return Inside;
};

const styles = StyleSheet.create({
  base: {
    fontFamily: gStyle.fontFamily,
  },
  rtl: {
    direction: Platform.OS === 'ios' ? 'ltr' : 'rtl',
    textAlign: 'right',
  },
});

const variationStyles = StyleSheet.create<
  {
    [k in TextVariations]: TextStyle;
  }
>({
  h1: {
    fontWeight: 'bold',
    fontSize: gStyle.fontSizes.h1,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: gStyle.fontSizes.h2,
  },
  b1: {
    fontSize: gStyle.fontSizes.b1,
  },
  b2: {
    fontSize: gStyle.fontSizes.b2,
    fontWeight: '100',
  },
  b3: {
    fontSize: gStyle.fontSizes.b3,
    fontWeight: '100',
  },
  link: {
    fontSize: gStyle.fontSizes.link,
    textDecorationLine: 'underline',
    color: gStyle.colors.lowContrastDark,
  },
  error: {
    color: gStyle.colors.error,
    fontSize: gStyle.fontSizes.error,
  },
});
