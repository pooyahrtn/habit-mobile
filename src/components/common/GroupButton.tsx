import React, { memo } from 'react';
import { StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import { Text } from './Text';
import {
  View as AnimatableView,
  CustomAnimation,
} from 'react-native-animatable';
import { gStyle } from '@components/styles';

export interface GroupButtonProps<K extends string = string> {
  items: {
    key: K;
    value: string;
  }[];
  selectedKey?: K;
  onChange: (key: K) => void;
  containerStyle?: ViewStyle;
}

export const GroupButton = memo((props: GroupButtonProps) => {
  const { containerStyle, items, selectedKey, onChange } = props;
  return (
    <View style={[styles.container, containerStyle]}>
      {items.map(({ key, value }) => (
        <AnimatableView
          style={styles.item}
          key={key}
          easing="ease-in-cubic"
          duration={200}
          animation={
            !selectedKey
              ? undefined
              : selectedKey !== key
              ? TO_INACTIVE_ANIMATION
              : TO_ACTIVE_ANIMATION
          }>
          <TouchableOpacity
            style={styles.itemTouchable}
            onPress={() => onChange(key)}>
            <Text
              variation="b1"
              style={[
                styles.itemText,
                selectedKey === key && styles.itemTextActive,
              ]}>
              {value}
            </Text>
          </TouchableOpacity>
        </AnimatableView>
      ))}
    </View>
  );
});

const TO_ACTIVE_ANIMATION: CustomAnimation = {
  from: {
    backgroundColor: gStyle.colors.background,
  },
  to: {
    backgroundColor: gStyle.colors.primary,
  },
};

const TO_INACTIVE_ANIMATION: CustomAnimation = {
  from: {
    backgroundColor: gStyle.colors.primary,
  },
  to: {
    backgroundColor: gStyle.colors.background,
  },
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-evenly',
    paddingStart: gStyle.layouts.s,
    paddingVertical: gStyle.layouts.s,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: gStyle.colors.lowContrastDark,
  },
  item: {
    flex: 1,
    marginStart: gStyle.layouts.s,
    borderRadius: 10,
    backgroundColor: gStyle.colors.background,
    height: 40,
  },
  itemTouchable: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemText: {
    fontWeight: 'bold',
    color: gStyle.colors.lowContrastDark,
  },
  itemTextActive: {
    color: gStyle.colors.highContrastLight,
  },
});
