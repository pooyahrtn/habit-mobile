import React from 'react';
import { View, StyleSheet } from 'react-native';
import Image from 'react-native-fast-image';
const BottomRight = require('../../assets/images/decorator/bottom-right.png');
const TopLeft = require('../../assets/images/decorator/top-left.png');

interface DecoratorImageProps {
  variation: 'bottom-righ' | 'top-left';
}

export const DecoratorImage = (props: DecoratorImageProps) => {
  const { variation } = props;
  return (
    <View
      style={[
        styles.containerBase,
        variation === 'bottom-righ' && styles.bottomRight,
        variation === 'top-left' && styles.topLeft,
      ]}>
      {variation === 'bottom-righ' && (
        <Image source={BottomRight} resizeMode="contain" style={styles.image} />
      )}
      {variation === 'top-left' && (
        <Image source={TopLeft} resizeMode="stretch" style={styles.image} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  containerBase: {
    aspectRatio: 1,
    position: 'absolute',
    width: 130,
    height: 130,
  },
  bottomRight: {
    bottom: 0,
    right: 0,
    alignItems: 'flex-end',
  },
  topLeft: {
    top: 0,
    left: 0,
  },
  image: {
    height: '100%',
    width: '90%',
  },
});
