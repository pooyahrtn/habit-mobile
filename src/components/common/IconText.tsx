import React from 'react';
import { View, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { Icon, IonNames } from './Icon';
import { Text } from './Text';
import { gStyle } from '@components/styles';

interface IconTextProps {
  containerStyle?: ViewStyle;
  iconName: IonNames;
  color?: string;
  text: string;
  textStyle?: TextStyle;
}
export const IconText = ({
  containerStyle,
  iconName,
  color,
  text,
  textStyle,
}: IconTextProps) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <Icon name={iconName} color={color} />
      <Text variation="b1" style={[styles.text, { color }, textStyle]}>
        {text}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    marginVertical: gStyle.layouts.m,
  },
  text: {
    marginRight: gStyle.layouts.m,
  },
});
