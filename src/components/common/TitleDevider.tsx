import React from 'react';
import { View, ViewStyle, StyleSheet, ViewProps } from 'react-native';
import { Text } from './Text';
import { gStyle } from '../styles';

export interface TitleDeviderProps extends ViewProps {
  title: string;
  lineStyle?: ViewStyle;
}
export const TitleDevider = (props: TitleDeviderProps) => {
  const { title, lineStyle, style } = props;
  return (
    <View style={[styles.container, style]}>
      <Text variation="b2" style={styles.textStyle}>
        {title}
      </Text>
      <View style={[styles.line, lineStyle]} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    marginVertical: gStyle.layouts.l,
  },
  line: {
    flex: 1,
    marginRight: gStyle.layouts.m,
    height: 1,
    backgroundColor: gStyle.colors.devider,
  },
  textStyle: {
    color: gStyle.colors.lowContrastDark,
  },
});
