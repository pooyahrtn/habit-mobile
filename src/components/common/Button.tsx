import React, { PropsWithChildren } from 'react';
import {
  TouchableOpacity,
  ActivityIndicator,
  TouchableOpacityProps,
  StyleSheet,
  ActivityIndicatorProps,
  TextStyle,
} from 'react-native';
import { Text } from './Text';
import { Icon, IonNames } from './Icon';
import mixedStyles, { Color, TextVariations, gStyle } from '../styles';
import { getColorStyle } from './utils';

export interface ButtonProps extends TouchableOpacityProps {
  loading?: boolean;
  indicatorProps?: ActivityIndicatorProps;
  indicatorColor?: string;
  content?: ButtonContentProps;
  rounded?: boolean;
  backgroundColor?: Color;
  disabled?: boolean;
}

export const Button = (props: PropsWithChildren<ButtonProps>) => {
  const {
    onPress,
    loading,
    indicatorProps,
    children,
    style,
    indicatorColor = 'white',
    content,
    rounded = true,
    backgroundColor = 'primary',
    disabled,
  } = props;
  const _onPress = (e: any) => {
    if (!loading && !disabled && onPress) {
      onPress(e);
    }
  };
  return (
    <TouchableOpacity
      onPress={_onPress}
      style={[
        styles.base,
        mixedStyles.shadowSmall,
        mixedStyles.shadowCenter,
        rounded && styles.round,
        getColorStyle(backgroundColor).background,
        disabled && styles.disabledContainer,
        style,
      ]}>
      {props.loading ? (
        <ActivityIndicator color={indicatorColor} {...indicatorProps} />
      ) : content ? (
        <ButtonContent {...content} disabled={disabled} />
      ) : (
        children
      )}
    </TouchableOpacity>
  );
};

interface ButtonContentProps {
  text?: string;
  iconStart?: IonNames;
  color?: Color;
  iconEnd?: IonNames;
  variation?: TextVariations;
  textStyle?: TextStyle;
  disabled?: boolean;
}
const ButtonContent = (props: ButtonContentProps) => {
  const {
    text,
    color = 'highContrastLight',
    iconEnd,
    iconStart,
    textStyle,
    variation = 'b1',
    disabled,
  } = props;
  return (
    <>
      {iconStart && (
        <Icon
          name={iconStart}
          style={styles.endIconStyle}
          color={
            disabled ? gStyle.colors.lowContrastDark : gStyle.colors[color]
          }
        />
      )}
      <Text
        variation={variation}
        style={[
          styles.textStyle,
          getColorStyle(color).color,
          textStyle,
          disabled && styles.disabledText,
        ]}>
        {text}
      </Text>
      {iconEnd && (
        <Icon
          name={iconEnd}
          style={styles.endIconStyle}
          color={disabled ? gStyle.colors.disabledDark : gStyle.colors[color]}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  base: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 50,
    paddingHorizontal: gStyle.layouts.xl,
    borderWidth: 0,
  },
  disabledContainer: {
    backgroundColor: gStyle.colors.background,
    borderWidth: 2,
    borderColor: gStyle.colors.disabledDark,
    shadowOpacity: 0,
    elevation: 0,
  },
  disabledText: {
    color: gStyle.colors.disabledDark,
  },
  round: { borderRadius: 200 },
  textStyle: {
    fontWeight: 'bold',
  },
  endIconStyle: {
    marginStart: 10,
  },
});
