import React, { PropsWithChildren } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  ViewProps,
  View,
  Platform,
} from 'react-native';
// import { getLayoutSize } from './utils';
import { Layouts, gStyle } from '../styles';

type Paddings = 'start' | 'end' | 'horizontal' | 'none';
export interface SafeAreaProps extends ViewProps {
  padding?: Paddings;
  size?: Layouts;
  decorator?: any;
}
export const SafeArea = (props: PropsWithChildren<SafeAreaProps>) => {
  const { style, decorator } = props;
  return (
    <SafeAreaView style={baseStyle.base}>
      <View
        style={[
          baseStyle.base,
          style,
          Platform.OS === 'android' && baseStyle.androidContainer,
        ]}>
        {props.children}
      </View>
      {decorator}
    </SafeAreaView>
  );
};

const baseStyle = StyleSheet.create({
  base: {
    flex: 1,
    backgroundColor: gStyle.colors.background,
  },
  androidContainer: { paddingTop: StatusBar.currentHeight },
});
