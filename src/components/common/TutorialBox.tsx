import React from 'react';
import { View, StyleSheet, ViewStyle, TouchableOpacity } from 'react-native';
import { Text } from './Text';
import { Icon } from './Icon';
import { gStyle } from '@components/styles';

interface TutorialBoxProps {
  onClose: () => void;
  text: string;
  containerStyle?: ViewStyle;
}
export const TutorialBox = (props: TutorialBoxProps) => {
  const { onClose, text, containerStyle } = props;
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.textContainar}>
        <Text variation="b2">{text}</Text>
      </View>
      <TouchableOpacity onPress={onClose} style={styles.closeContainer}>
        <Icon name="md-close" color={gStyle.colors.lowContrastDark} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    marginHorizontal: gStyle.layouts.l,
    borderWidth: 2,
    borderColor: gStyle.colors.primaryLighter,
    borderRadius: gStyle.layouts.l,
  },
  textContainar: {
    flex: 1,
    padding: gStyle.layouts.m,
  },
  closeContainer: {
    padding: gStyle.layouts.m,
  },
});
