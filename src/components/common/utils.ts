import { gStyle, Color, Layouts } from '../styles';
import { StyleSheet, ViewStyle } from 'react-native';

export const getColorStyle = (color: Color) =>
  StyleSheet.create({
    background: {
      backgroundColor: gStyle.colors[color],
    },
    color: {
      color: gStyle.colors[color],
    },
  });

type Spacings = 'm' | 'mv' | 'mh';

export const getSpacingStyles = (size: Layouts) => (
  spacing: Spacings,
): ViewStyle => {
  switch (spacing) {
    case 'm':
      return {
        margin: gStyle.layouts[size],
      };
    case 'mv':
      return {
        marginVertical: gStyle.layouts[size],
      };
  }
  return {};
};
