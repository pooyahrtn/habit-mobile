import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon, IonNames } from './Icon';
import { gStyle } from '@components/styles';

interface TabIcon {
  name: IonNames;
  active: boolean;
  hasNotification: boolean;
}
export const TabIcon = (props: TabIcon) => {
  const { name, active, hasNotification } = props;

  return (
    <View style={styles.container}>
      <Icon
        name={name}
        color={active ? gStyle.colors.primary : gStyle.colors.lowContrastDark}
        size={28}
      />
      {hasNotification && <View style={styles.notificationCircle} />}
    </View>
  );
};

const CONTAINER_SIZE = 38;
const NOTIFICATION_CIRCLE_D = 10;
const styles = StyleSheet.create({
  container: {
    width: CONTAINER_SIZE,
    height: CONTAINER_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notificationCircle: {
    width: NOTIFICATION_CIRCLE_D,
    height: NOTIFICATION_CIRCLE_D,
    borderRadius: NOTIFICATION_CIRCLE_D / 2,
    backgroundColor: 'red',
    position: 'absolute',
    top: 3,
    left: 0,
    // ...theme.shadow.shadowSmall,
    // ...theme.shadow.shadowCenter,
  },
});
