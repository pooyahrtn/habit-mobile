import React from 'react';
import { Icon, IonNames } from './Icon';
import { ViewStyle, StyleSheet, TouchableOpacity } from 'react-native';
import { gStyle } from '@components/styles';

interface CloseIconProps {
  onPress: () => void;
  containerStyle?: ViewStyle;
  iconName?: IonNames;
}
export const IconButton = (props: CloseIconProps) => {
  const { onPress, containerStyle, iconName = 'md-close' } = props;
  return (
    <TouchableOpacity
      style={[styles.container, containerStyle]}
      onPress={onPress}>
      <Icon
        name={iconName}
        style={styles.icon}
        color={gStyle.colors.lowContrastDark}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: gStyle.layouts.m,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    textAlign: 'center',
  },
});
