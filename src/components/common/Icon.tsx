import React from 'react';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { TextStyle } from 'react-native';

export type IonNames =
  | 'md-arrow-round-forward'
  | 'md-arrow-round-back'
  | 'md-home'
  | 'md-compass'
  | 'md-flag'
  | 'md-ribbon'
  | 'md-add'
  | 'md-settings'
  | 'md-close'
  | 'md-square-outline'
  | 'md-checkbox-outline'
  | 'md-people'
  | 'md-checkmark'
  | 'md-calendar'
  | 'md-finger-print'
  | 'md-more';

type Props = {
  name: IonNames;
  size?: number;
  color?: string;
  iconType?: 'ion-icon';
  style?: TextStyle;
  offset?: number;
};

export const Icon = (props: Props) => {
  // const dOffset = 0;
  const iconProps = {
    size: props.size ? props.size : 24,
    style: {
      ...props.style,
    },
    color: props.color ? props.color : 'black',
  };

  return <IonIcon {...iconProps} {...props} />;
};
