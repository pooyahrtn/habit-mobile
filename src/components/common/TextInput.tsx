import React from 'react';
import {
  TextInput as NativeInput,
  TextInputProps as NativeProps,
  View,
  StyleSheet,
  ViewStyle,
  Platform,
} from 'react-native';
import { Text } from './Text';
import { gStyle } from '../styles';

export interface TextInputProps extends NativeProps {
  containerStyle?: ViewStyle;
  error?: string;
  unit?: string;
}
export const TextInput = (props: TextInputProps) => {
  const { containerStyle, error, unit } = props;
  return (
    <View style={containerStyle}>
      <View style={[styles.container]}>
        <NativeInput
          placeholderTextColor={gStyle.colors.lowContrastDark}
          {...props}
          style={[styles.textInputStyle, props.style]}
        />
        {unit && <Text variation="b2">{unit}</Text>}
      </View>
      {error && (
        <Text variation="error" style={styles.errorStyle}>
          {error}
        </Text>
      )}
    </View>
  );
};

const MIN_HEIGHT = 50;
const styles = StyleSheet.create({
  container: {
    borderRadius: MIN_HEIGHT / 2,
    minHeight: MIN_HEIGHT,
    borderWidth: 1,
    borderColor: gStyle.colors.lowContrastDark,
    paddingHorizontal: MIN_HEIGHT / 2,
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  textInputStyle: {
    direction: Platform.OS === 'ios' ? 'ltr' : 'rtl',
    textAlign: 'right',
    flex: 1,
    fontFamily: gStyle.fontFamily,
    fontSize: gStyle.fontSizes.b1,
  },
  errorStyle: {
    marginHorizontal: MIN_HEIGHT / 2,
    marginTop: gStyle.layouts.m,
  },
});
