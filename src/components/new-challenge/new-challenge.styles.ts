import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export default StyleSheet.create({
  display: {
    padding: gStyle.layouts.l,
  },
  bottomRowContainer: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonRow: {
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 2,
    backgroundColor: 'white',
    padding: gStyle.layouts.s,
    borderRadius: gStyle.layouts.xl,
  },
  backButtonContainer: {
    marginHorizontal: gStyle.layouts.l,
    alignItems: 'center',
    flexDirection: 'row',
  },
  backButtonText: {
    marginHorizontal: gStyle.layouts.s,
  },
  scroll: {
    flex: 1,
  },
});
