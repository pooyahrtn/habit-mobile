import React from 'react';
import { Text } from '@components/common';
import * as locale from '../../locale';
import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export const NewChallengeExplain = () => {
  return (
    <>
      <Text variation="h1" style={styles.text}>
        چالش جدید بساز
      </Text>
      <Text variation="b1" style={styles.text}>
        {locale.challenges.createNewChallengeDescription}
      </Text>
      <Text variation="b2" style={styles.text}>
        یادت باشه، هدف ما کمک کردن به آدم هاست، و چالش ها باید در راستای بهبود
        زندگی باش.
      </Text>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: gStyle.layouts.l,
  },
});
