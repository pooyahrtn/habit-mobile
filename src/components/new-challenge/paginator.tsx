import React from 'react';
import { View, StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export interface PaginatorProps {
  n: number;
  activeIndex: number;
}
export const Paginator = ({ n, activeIndex }: PaginatorProps) => {
  return (
    <View style={styles.container}>
      {[...Array(n).keys()].map((k) => (
        <View
          key={k + ''}
          style={[styles.dot, k === activeIndex && styles.dotActive]}
        />
      ))}
    </View>
  );
};

const DOT_SIZE = 7;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  dot: {
    width: DOT_SIZE,
    height: DOT_SIZE,
    borderRadius: DOT_SIZE,
    marginHorizontal: DOT_SIZE / 3,
    backgroundColor: gStyle.colors.devider,
  },
  dotActive: {
    backgroundColor: gStyle.colors.highContrastDark,
  },
});
