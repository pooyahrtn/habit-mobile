import React from 'react';

import { TitleDevider, Text } from '@components/common';
import { toPersian } from '@utils/text';
import { gStyle } from '@components/styles';
import { StyleSheet, View } from 'react-native';
import { FormItem } from '@utils/forms';

interface NewChallengeConfirmProps {
  items: FormItem[];
  state: any;
}
export const NewChallengeConfirm = (props: NewChallengeConfirmProps) => {
  const { items, state } = props;

  return (
    <View style={styles.container}>
      <Text variation="h1">مرور اطلاعت</Text>
      <Text variation="b1">آیا اطلاعات زیر را تایید میکنید؟</Text>
      {items.map((i) => (
        <NewChallengeConfirmItem key={i.title} item={i} value={state[i.key]} />
      ))}
    </View>
  );
};

interface NewChallengeConfirmItemProps {
  item: FormItem;
  value: string;
}
const NewChallengeConfirmItem = (props: NewChallengeConfirmItemProps) => {
  const {
    item: { title, type, text, group },
    value,
  } = props;
  return (
    <>
      <TitleDevider title={title} />

      {type === 'text' && (
        <Text variation="b1">
          {toPersian(value) + ' ' + (text?.unit ?? '')}
        </Text>
      )}
      {type === 'group' && (
        <Text variation="b1" style={styles.bold}>
          {group?.items.find((g) => g.key === value)?.value ?? ''}
        </Text>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  errorText: { color: gStyle.colors.error },
  container: {
    marginBottom: gStyle.layouts.l,
  },
  bold: {
    fontWeight: 'bold',
  },
});
