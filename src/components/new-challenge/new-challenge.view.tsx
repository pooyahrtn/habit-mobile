import React, { useState } from 'react';
import { View } from 'react-native';
import { SafeArea, Button, Text, Icon } from '@components/common';
import styles from './new-challenge.styles';
import { NewChallengeExplain } from './new-challenge-explain';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Paginator } from './paginator';
import { gStyle } from '@components/styles';
import { NewChallengeInput } from './new-challenge-input';
import { NewChallengeConfirm } from './new-challenge-confirm';
import { FormItem } from '@utils/forms';

interface NewChallengeViewProps<S> {
  currentPage: number;
  onNextPressed: () => void;
  onBackPressed: () => void;
  firstInput: FormItem<any>[];
  secondInput: FormItem<any>[];
  loading: boolean;
  state: S;
  onStateChange: (key: keyof S) => (value: string) => void;
}

export function NewChallengeView<S>(props: NewChallengeViewProps<S>) {
  const {
    currentPage,
    onBackPressed,
    onNextPressed,
    firstInput,
    secondInput,
    state,
    onStateChange,
    loading,
  } = props;

  const [firstSlideFine, setFirstSlideFine] = useState(false);
  const [secondSlideFine, setSecondSlideFine] = useState(false);

  return (
    <SafeArea style={styles.display}>
      <ScrollView style={styles.scroll}>
        {currentPage === 0 && <NewChallengeExplain />}
        {currentPage === 1 && (
          <NewChallengeInput
            fields={firstInput}
            state={state}
            onValueChange={onStateChange}
            setIsFine={setFirstSlideFine}
          />
        )}
        {currentPage === 2 && (
          <NewChallengeInput
            fields={secondInput}
            state={state}
            onValueChange={onStateChange}
            setIsFine={setSecondSlideFine}
          />
        )}
        {currentPage === 3 && (
          <NewChallengeConfirm
            items={[...firstInput, ...secondInput]}
            state={state}
          />
        )}
      </ScrollView>
      <View style={styles.bottomRowContainer}>
        <View style={styles.buttonRow}>
          {currentPage > 0 && (
            <TouchableOpacity
              style={styles.backButtonContainer}
              onPress={onBackPressed}>
              <Icon
                name="md-arrow-round-back"
                color={gStyle.colors.lowContrastDark}
              />
              <Text variation="b2" style={styles.backButtonText}>
                قبلی
              </Text>
            </TouchableOpacity>
          )}

          <Button
            onPress={onNextPressed}
            loading={loading}
            disabled={
              (currentPage === 1 && !firstSlideFine) ||
              (currentPage === 2 && !secondSlideFine)
            }
            content={{ text: 'تایید', iconEnd: 'md-arrow-round-forward' }}
          />
        </View>
        <Paginator n={4} activeIndex={currentPage} />
      </View>
    </SafeArea>
  );
}
