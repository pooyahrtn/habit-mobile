import React, { useState } from 'react';
import { FormContainer } from './new-challenge.form';
import { FormItem } from '@utils/forms';
import { View, StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

interface Props<S> {
  fields: FormItem<S>[];
  state: S;
  onValueChange: (key: keyof S) => (value: string) => void;
  setIsFine: (isFine: boolean) => void;
}
export function NewChallengeInput<S>({
  fields,
  state,
  onValueChange,
  setIsFine,
}: Props<S>) {
  const [errors, setErrors] = useState<any>({});
  const checkIfFine = () => {
    const fieldWithErrors = fields.filter(
      (f) => f.errors && f.errors(state[f.key] as any),
    );
    const isFine = fieldWithErrors.length === 0;
    setIsFine(isFine);
  };
  return (
    <View style={styles.container}>
      {fields.map((f) => (
        <FormContainer
          form={f}
          value={state[f.key] as any}
          onValueChange={(value: any) => {
            setErrors({ ...errors, [f.key]: undefined });
            onValueChange(f.key as any)(value);
            checkIfFine();
          }}
          error={errors[f.key]}
          checkForError={() => {
            if (f.errors) {
              const e = f.errors(state[f.key] as any);
              setErrors({ ...errors, [f.key]: e });
            }
          }}
        />
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: gStyle.layouts.xl,
  },
});
