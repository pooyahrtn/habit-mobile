import React from 'react';
import { StyleSheet, View } from 'react-native';
import { gStyle } from '@components/styles';
import { TitleDevider, Text, TextInput, GroupButton } from '@components/common';
import { FormItem } from '@utils/forms';

export function FormContainer<S>(props: {
  form: FormItem<S>;
  value: string;
  onValueChange: (v: string) => void;
  error?: string;
  checkForError: () => void;
}) {
  const {
    form: { title, description, type },
    form,
    value,
    onValueChange,
    error,
    checkForError,
  } = props;

  return (
    <View style={styles.container}>
      <TitleDevider title={title} style={styles.titleDevider} />
      <Text variation="b1" style={styles.descriptionText}>
        {description}
      </Text>
      {type === 'text' && (
        <FormTextInput
          form={form}
          value={value}
          onValueChange={onValueChange}
          onBlur={checkForError}
        />
      )}
      {type === 'group' && (
        <FormGroup form={form} value={value} onValueChange={onValueChange} />
      )}
      {error && <Text variation="error">{error}</Text>}
    </View>
  );
}

export function FormTextInput(props: {
  form: FormItem<any>;
  value: string;
  onValueChange: (v: string) => void;
  onBlur: () => void;
}) {
  const {
    form: { text },
    value,
    onValueChange,
    onBlur,
  } = props;
  if (!text) {
    throw new Error('text not provided');
  }
  const { placeholder, unit, maxLength, numberOfLines, inputType } = text;
  return (
    <TextInput
      placeholder={placeholder}
      unit={unit}
      multiline={(numberOfLines ?? 0) > 1}
      numberOfLines={numberOfLines}
      maxLength={maxLength}
      keyboardType={inputType === 'number' ? 'numeric' : 'default'}
      value={value}
      onChangeText={onValueChange}
      onBlur={onBlur}
    />
  );
}

export function FormGroup(props: {
  form: FormItem<any>;
  value: string;
  onValueChange: (v: string) => void;
}) {
  const {
    form: { group },
    value,
    onValueChange,
  } = props;
  if (!group) {
    throw new Error('group not provided');
  }
  const { items } = group;
  return (
    <GroupButton items={items} selectedKey={value} onChange={onValueChange} />
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: gStyle.layouts.m,
  },
  titleDevider: {
    marginBottom: gStyle.layouts.m,
  },
  descriptionText: {
    marginBottom: gStyle.layouts.m,
  },
  errorText: {
    color: gStyle.colors.error,
  },
});
