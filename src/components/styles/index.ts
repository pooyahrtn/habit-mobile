import { StyleSheet, ViewStyle } from 'react-native';
import { gStyle, Color } from './config';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  primaryBack: {
    backgroundColor: gStyle.colors.primary,
  },
  primaryText: {
    color: gStyle.colors.primary,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  alignCenter: {
    alignItems: 'center',
  },
  shadowSmall: {
    elevation: 4,
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  shadowMedium: {
    elevation: 4,
    shadowOpacity: 0.6,
    shadowRadius: 8,
  },
  shadowCenter: {
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
});

export * from './config';
