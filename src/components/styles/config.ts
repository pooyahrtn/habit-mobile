export type Color =
  | 'background'
  | 'primary'
  | 'primaryLighter'
  | 'primaryDarker'
  | 'highContrastLight'
  | 'lowContrastLight'
  | 'highContrastDark'
  | 'lowContrastDark'
  | 'disabledDark'
  | 'error'
  | 'devider';

export type TextVariations =
  | 'h1'
  | 'h2'
  | 'b1'
  | 'b2'
  | 'b3'
  | 'link'
  | 'error';
export type Layouts = 's' | 'm' | 'l' | 'xl';

interface Config {
  fontFamily: string;
  fontSizes: {
    [k in TextVariations]: number;
  };
  colors: {
    [k in Color]: string;
  };
  layouts: {
    [k in Layouts]: number;
  };
  shadows: {
    xsmall: object;
    small: object;
    medium: object;
    center: object;
  };
}

export const gStyle: Config = {
  fontFamily: 'Anjoman',
  fontSizes: {
    h1: 30,
    h2: 20,
    b1: 16,
    b2: 15,
    b3: 14,
    link: 15,
    error: 15,
  },
  colors: {
    background: 'rgb(250,250,250)',
    primary: '#538DC3',
    primaryDarker: '#538DC3',
    primaryLighter: '#A2D0FB',
    highContrastDark: 'black',
    lowContrastDark: 'gray',
    highContrastLight: 'white',
    lowContrastLight: 'gray',
    disabledDark: 'rgb(200,200,200)',
    error: 'red',
    devider: 'rgb(200,200,200)',
  },
  layouts: {
    s: 5,
    m: 10,
    l: 20,
    xl: 30,
  },
  shadows: {
    xsmall: {
      elevation: 2,
      shadowOpacity: 0.1,
      shadowRadius: 2,
    },
    small: {
      elevation: 4,
      shadowOpacity: 0.2,
      shadowRadius: 4,
    },
    medium: {
      elevation: 4,
      shadowOpacity: 0.6,
      shadowRadius: 8,
    },
    center: {
      shadowOffset: {
        width: 0,
        height: 0,
      },
    },
  },
};
