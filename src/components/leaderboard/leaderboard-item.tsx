import React from 'react';
import { View, StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';
import Image from 'react-native-fast-image';
import { Text } from '@components/common';
import { toPersian } from '@utils/text';
import * as locale from '../../locale';

export interface LeaderboardItemProps {
  id: string;
  image: string;
  username: string;
  score: number;
}
export const LeaderboardItem = ({
  image,
  username,
  score,
}: LeaderboardItemProps) => {
  return (
    <View style={styles.itemContainer}>
      <View style={styles.textContainer}>
        <Text variation="b1">{username}</Text>
        <Text variation="b1" style={styles.scoreText}>
          {locale.score.withScore(score)}
        </Text>
      </View>
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: image }}
          style={styles.image}
          resizeMode="contain"
        />
      </View>
    </View>
  );
};

const AVATAR_SIZE = 54;
const AVATAR_CONTAINER = 70;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    paddingVertical: gStyle.layouts.m,
    alignItems: 'center',
  },
  avatarContainer: {
    width: AVATAR_CONTAINER,
    height: AVATAR_CONTAINER,
    backgroundColor: 'white',
    borderRadius: AVATAR_CONTAINER,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  image: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
  },
  textContainer: {
    flex: 1,
    marginHorizontal: gStyle.layouts.l,
    justifyContent: 'center',
  },
  scoreText: {
    color: gStyle.colors.primary,
  },
});
