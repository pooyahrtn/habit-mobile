import React from 'react';
import { FlatList } from 'react-native';
import { SafeArea, DecoratorImage, Text } from '@components/common';
import styles from './leaderboard.styles';
import { LeaderboardItem, LeaderboardItemProps } from './leaderboard-item';

export interface LeaderboardViewProps {
  items: LeaderboardItemProps[];
}
export const LeaderboardView = ({ items }: LeaderboardViewProps) => {
  return (
    <SafeArea
      decorator={<DecoratorImage variation="top-left" />}
      style={styles.screen}>
      <Text variation="h1" style={styles.title}>
        نفرات برتر
      </Text>
      <FlatList
        style={styles.list}
        data={items}
        renderItem={({ item }) => <LeaderboardItem {...item} />}
      />
    </SafeArea>
  );
};
