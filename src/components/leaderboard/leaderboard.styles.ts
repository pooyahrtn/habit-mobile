import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export default StyleSheet.create({
  screen: {
    paddingHorizontal: gStyle.layouts.l,
  },
  title: {
    marginVertical: gStyle.layouts.l,
  },
  list: {
    flex: 1,
  },
});
