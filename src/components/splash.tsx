import React from 'react';
import { View, StyleSheet } from 'react-native';

export const SplashScreen = () => {
  return <View style={styles.screen} />;
};

const styles = StyleSheet.create({
  screen: { flex: 1 },
});
