import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Text, Icon } from '@components/common';
import { gStyle } from '@components/styles';
import { CircularProgress } from 'react-native-circular-progress';
import Image from 'react-native-fast-image';
import * as locale from '../../locale';

export interface ProfileHeaderProps {
  score: number;
  level: number;
  image?: string;
  progress: number;
  onSettingPressed: () => void;
}
export const ProfileHeader = (props: ProfileHeaderProps) => {
  const { score, level, image, progress, onSettingPressed } = props;
  return (
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        <CircularProgress
          size={PROGRESS_SIZE}
          width={5}
          fill={progress * 100}
          style={styles.progressbar}
          tintColor="#C3F2BA"
          rotation={20}
          lineCap="round"
          onAnimationComplete={() => console.log('onAnimationComplete')}
          // backgroundColor={gStyle.colors.background}
        />
        <View style={styles.avatarImageContainer}>
          <Image
            source={{ uri: image }}
            style={styles.avatarImage}
            resizeMode="contain"
          />
        </View>
      </View>
      <View style={styles.textsContainer}>
        <Text variation="b2" style={styles.scoreText}>
          {locale.score.withScore(score)}
        </Text>
        <Text variation="b2" style={styles.scoreText}>
          {locale.score.withLevel(level)}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.settingsButton}
        onPress={onSettingPressed}>
        <Icon
          name="md-settings"
          color={gStyle.colors.lowContrastDark}
          size={26}
        />
      </TouchableOpacity>
    </View>
  );
};

const AVATAR_SIZE = 60;
const PROGRESS_SIZE = 80;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    padding: gStyle.layouts.l,
    // paddingBottom: 0,
  },
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: PROGRESS_SIZE,
    height: PROGRESS_SIZE,
  },
  avatarImageContainer: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE,
    // overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    ...gStyle.shadows.center,
    ...gStyle.shadows.small,
  },
  avatarImage: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE,
  },
  progressbar: {
    position: 'absolute',
  },
  textsContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: gStyle.layouts.m,
  },
  scoreText: {
    fontSize: 18,
    color: gStyle.colors.lowContrastDark,
    // marginTop: gStyle.layouts.s,
  },
  settingsButton: {
    padding: gStyle.layouts.s,
  },
});
