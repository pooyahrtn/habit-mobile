import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import {
  View as AnimatableView,
  CustomAnimation,
} from 'react-native-animatable';
import { Text, Icon } from '@components/common';
import { gStyle } from '@components/styles';
import { RemindTime } from '@graphql/types/graphql-global-types';
// import * as locale from '../../locale';
export interface TaskItemProps {
  remindText: string;
  reward: number;
  done: boolean;
  id: string;
  remindTime: RemindTime;
  onDone: (id: string) => void;
}

export const TaskItem = (props: TaskItemProps) => {
  const { remindText, done, id, onDone } = props;
  const [state, setState] = useState<'not-done' | 'done' | 'to-done'>(
    done ? 'done' : 'not-done',
  );

  const onDonePressed = () => {
    onDone(id);
    if (state === 'not-done') {
      setState('to-done');
    }
  };

  return (
    <AnimatableView
      style={[styles.taskAnimatedView, done && styles.doneContainer]}
      animation={state === 'to-done' ? TO_DONE_ANIMATION : undefined}>
      <TouchableOpacity
        style={styles.taskItemContainer}
        onPress={onDonePressed}>
        <Text
          variation="b1"
          style={[
            styles.taskItemText,
            (state !== 'not-done' || done) && styles.lightText,
          ]}>
          {remindText}
        </Text>

        <Icon
          name={done ? 'md-checkbox-outline' : 'md-square-outline'}
          color={state === 'not-done' ? 'black' : 'white'}
        />
      </TouchableOpacity>
    </AnimatableView>
  );
};

const DONE_COLOR = '#3AC08E';

const TO_DONE_ANIMATION: CustomAnimation = {
  from: {
    backgroundColor: 'white',
  },
  to: {
    backgroundColor: DONE_COLOR,
  },
};

const styles = StyleSheet.create({
  taskAnimatedView: {
    marginHorizontal: gStyle.layouts.l,
    marginVertical: gStyle.layouts.s,
    backgroundColor: 'white',
    borderRadius: gStyle.layouts.m,
    ...gStyle.shadows.xsmall,
    ...gStyle.shadows.center,
  },
  taskItemContainer: {
    flexDirection: 'row-reverse',
    width: '100%',
    minHeight: 56,
    // padding: gStyle.layouts.m,
    padding: 13,
    borderRadius: gStyle.layouts.m,
    alignItems: 'center',
  },
  taskItemText: {
    flex: 1,
    color: 'black',
  },
  lightText: {
    color: 'white',
  },
  doneContainer: {
    backgroundColor: DONE_COLOR,
  },
  rewardText: {
    flex: 1,
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
  },
});
