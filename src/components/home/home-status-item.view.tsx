import React from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { gStyle } from '@components/styles';
import { Text, Icon } from '@components/common';
import { toPersian } from '@utils/text';

export interface HomeStatusItemProps {
  title: string;
  goal: string;
  nCompleted: number;
  repeatCount: number;
  id: string;
  onPress: (id: string) => void;
}

export const HomeStatusItem = (props: HomeStatusItemProps) => {
  const { title, goal, nCompleted, repeatCount, id, onPress } = props;
  return (
    <TouchableOpacity style={styles.container} onPress={() => onPress(id)}>
      <View style={styles.headerContainer}>
        <Text variation="h2">{title}</Text>
        <ProgressView nTotal={repeatCount} nCompleted={nCompleted} />
      </View>
      <View style={styles.goalContainer}>
        <Icon name="md-flag" style={styles.iconStyle} />
        <Text variation="b1">{goal}</Text>
      </View>
    </TouchableOpacity>
  );
};

interface PropgressViewProps {
  nTotal: number;
  nCompleted: number;
}
const ProgressView = ({ nCompleted, nTotal }: PropgressViewProps) => {
  return (
    <View style={styles.progressContainer}>
      {[...Array(nTotal)].map((_, i) => {
        const active = i < nCompleted;
        return (
          <React.Fragment key={`${i}`}>
            <View
              style={[styles.progressCircle, active && styles.progressActive]}>
              <Text
                variation="b3"
                style={[styles.numberText, active && styles.numberTextActive]}>
                {toPersian(i + 1 + '')}
              </Text>
            </View>
            {i !== nTotal - 1 && <View style={styles.progressLinkLine} />}
          </React.Fragment>
        );
      })}
    </View>
  );
};

const PROGRESS_CIRCLE_WIDTH = 16;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: gStyle.layouts.l,
    marginVertical: gStyle.layouts.m,
    backgroundColor: 'white',
    ...gStyle.shadows.small,
    ...gStyle.shadows.center,
    borderRadius: gStyle.layouts.l,
    padding: gStyle.layouts.l,
  },
  headerContainer: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginBottom: gStyle.layouts.m,
  },
  goalContainer: {
    flexDirection: 'row-reverse',
    paddingRight: gStyle.layouts.l,
  },
  iconStyle: {
    marginStart: gStyle.layouts.m,
  },
  progressContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    direction: 'ltr',
  },
  progressCircle: {
    borderRadius: PROGRESS_CIRCLE_WIDTH,
    width: PROGRESS_CIRCLE_WIDTH,
    height: PROGRESS_CIRCLE_WIDTH,
    borderWidth: 0.8,
    borderColor: gStyle.colors.lowContrastDark,
    justifyContent: 'center',
    alignItems: 'center',
  },
  progressActive: {
    backgroundColor: gStyle.colors.primaryDarker,
    borderWidth: 0,
    borderColor: 'transparent',
  },
  progressLinkLine: {
    height: 1,
    width: PROGRESS_CIRCLE_WIDTH / 4,
    backgroundColor: gStyle.colors.lowContrastDark,
  },
  numberText: {
    fontSize: 8,
    textAlign: 'center',
    fontWeight: 'bold',
    color: gStyle.colors.lowContrastDark,
  },
  numberTextActive: {
    color: 'white',
  },
});
