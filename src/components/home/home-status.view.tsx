import React from 'react';
import {} from 'react-native';
import { TutorialBox } from '@components/common';
import { HomeStatusItem, HomeStatusItemProps } from './home-status-item.view';
import * as locale from '../../locale';

export interface HomeStatusViewProps {
  hideTutorial: boolean;
  onTutorialRead: () => void;
  homeStatusItems: HomeStatusItemProps[];
}

export const HomeStatusView = (props: HomeStatusViewProps) => {
  const { hideTutorial, homeStatusItems, onTutorialRead } = props;
  return (
    <>
      {!hideTutorial && (
        <TutorialBox
          text={locale.home.statusTutorial}
          onClose={onTutorialRead}
        />
      )}
      {homeStatusItems.map((t) => (
        <HomeStatusItem key={t.id} {...t} />
      ))}
    </>
  );
};
