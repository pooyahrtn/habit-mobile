import React, { memo } from 'react';
import { TutorialBox } from '@components/common';
import * as locale from '../../locale';
import { TaskItemProps, TaskItem } from './task-item.view';

export interface HomeTasksViewProps {
  hideTutorial: boolean;
  onTutorialRead: () => void;
  tasks: TaskItemProps[];
}
export const HomeTaskView = memo((props: HomeTasksViewProps) => {
  const { hideTutorial, tasks, onTutorialRead } = props;
  return (
    <>
      {!hideTutorial && (
        <TutorialBox text={locale.home.taskTutorial} onClose={onTutorialRead} />
      )}
      {tasks.map((t) => (
        <TaskItem key={t.id} {...t} />
      ))}
    </>
  );
});

// interface TaskTimeSectionProps {
//   time: string;
//   tasks: TaskItemProps[];
// }
// const TaskTimeSection = (props: TaskTimeSectionProps) => {
//   const { tasks, time } = props;
//   return (
//     <>
//       <Text variation="h2" style={styles.sectionTitleText}>
//         {time}
//       </Text>
//       {tasks.map((t) => (
//         <TaskItem key={t.id} {...t} />
//       ))}
//     </>
//   );
// };

// const getSections = (tasks: TaskItemProps[]): TaskTimeSectionProps[] => {
//   const kVals = [
//     { key: RemindTime.Any, value: locale.time.any },
//     { key: RemindTime.Morning, value: locale.time.morning },
//     { key: RemindTime.Afternoon, value: locale.time.afternoon },
//     { key: RemindTime.Evening, value: locale.time.evening },
//     { key: RemindTime.Night, value: locale.time.night },
//   ];
//   return kVals.map((k) => ({
//     time: k.value,
//     tasks: tasks.filter((t) => t.remindTime === k.key),
//   }));
// };

// const styles = StyleSheet.create({
//   sectionTitleText: {
//     marginHorizontal: gStyle.layouts.l,
//   },
// });
