import React from 'react';
import { View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { SafeArea, DecoratorImage, Text } from '../common';
import { ProfileHeader, ProfileHeaderProps } from './profile-header';
import { gStyle } from '@components/styles';
import * as locale from '../../locale';
import { HomeTaskView, HomeTasksViewProps } from './home-tasks.view';
import { HomeStatusView, HomeStatusViewProps } from './home-status.view';
export type { HomeTasksViewProps } from './home-tasks.view';
export type { TaskItemProps } from './task-item.view';

export type HomeTab = 'today' | 'status';
interface HomeViewProps {
  header: ProfileHeaderProps;
  selectedTab: HomeTab;
  onTabSelect: (tab: HomeTab) => void;
  homeTaskProps: HomeTasksViewProps;
  homeStatusProps: HomeStatusViewProps;
}
export const HomeView = (props: HomeViewProps) => {
  const { header, selectedTab, onTabSelect } = props;
  return (
    <SafeArea>
      <DecoratorImage variation="bottom-righ" />
      <ScrollView>
        <ProfileHeader {...header} />
        <View style={styles.tabsRow}>
          <TabTitle
            text={locale.home.today}
            active={selectedTab === 'today'}
            onSelect={() => onTabSelect('today')}
          />
          <TabTitle
            text={locale.home.status}
            active={selectedTab === 'status'}
            onSelect={() => onTabSelect('status')}
          />
        </View>
        {selectedTab === 'today' && <HomeTaskView {...props.homeTaskProps} />}
        {selectedTab === 'status' && (
          <HomeStatusView {...props.homeStatusProps} />
        )}
      </ScrollView>
    </SafeArea>
  );
};

interface TabTitleProps {
  text: string;
  active: boolean;
  onSelect: () => void;
}
const TabTitle = (props: TabTitleProps) => {
  const { text, active, onSelect } = props;
  return (
    <TouchableOpacity
      onPress={onSelect}
      style={[styles.tabContainer, active && styles.tabContainerActive]}>
      <Text
        variation="h2"
        style={[active ? styles.tabTextActive : styles.tabText]}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  tabsRow: {
    flexDirection: 'row-reverse',
    borderBottomWidth: 1,
    borderColor: gStyle.colors.devider,
    margin: gStyle.layouts.l,
    marginTop: 0,
    paddingVertical: gStyle.layouts.m,
    alignItems: 'center',
  },
  tabContainer: {
    borderRadius: 100,
    paddingVertical: 3,
    paddingHorizontal: 20,
    backgroundColor: gStyle.colors.background,
    marginEnd: gStyle.layouts.m,
  },
  tabContainerActive: {
    backgroundColor: gStyle.colors.primary,
  },
  tabText: {
    color: gStyle.colors.lowContrastDark,
  },
  tabTextActive: {
    color: 'white',
  },
});
