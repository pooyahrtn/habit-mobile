import React from 'react';
import Modal from 'react-native-modal';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { Text } from './common';
import { gStyle } from './styles';

export interface ReportModalViewProps {
  visible: boolean;
  hideModal: () => void;
  onReportPressed: () => void;
}
export const ReportModalView = (props: ReportModalViewProps) => {
  const { visible, hideModal, onReportPressed } = props;
  return (
    <Modal isVisible={visible} style={styles.modal} onBackdropPress={hideModal}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={onReportPressed}>
          <Text variation="b1">گزارش چالش</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    padding: gStyle.layouts.l,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: gStyle.layouts.l,
  },
});
