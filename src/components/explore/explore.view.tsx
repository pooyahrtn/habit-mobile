import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { SafeArea, Text, Button } from '@components/common';
import { gStyle } from '@components/styles';
import * as locale from '../../locale';
import {
  ChallengeListItem,
  ChallengeListItemProps,
} from '@components/challenge/challenge-list-item';
import { CannotAcceptMoreView } from '@components/CannotAcceptMore';

interface ExploreViewProps {
  items: ChallengeListItemProps[];
  loading: boolean;
  showCannotAcceptMore: boolean;
  showAddButton: boolean;
  onAddButtonPressed: () => void;
}
export const ExploreView = (props: ExploreViewProps) => {
  const {
    items,
    showCannotAcceptMore,
    showAddButton,
    onAddButtonPressed,
  } = props;
  return (
    <SafeArea style={styles.screen}>
      <View style={styles.header}>
        <Text variation="h1">{locale.challenges.challengesTitle}</Text>
        {showAddButton && (
          <Button
            content={{
              text: 'ساخت چالش',
              iconEnd: 'md-add',
              color: 'lowContrastDark',
              textStyle: styles.buttonTextStyle,
            }}
            onPress={onAddButtonPressed}
            style={styles.buttonStyle}
          />
        )}
      </View>
      <FlatList
        style={styles.content}
        data={items}
        renderItem={({ item }) => <ChallengeListItem {...item} />}
      />
      {showCannotAcceptMore && <CannotAcceptMoreView />}
    </SafeArea>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: gStyle.layouts.l,
  },
  header: {
    flexDirection: 'row-reverse',
    marginVertical: gStyle.layouts.l,
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
  },
  buttonStyle: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: undefined,
    borderWidth: 1,
    borderColor: gStyle.colors.lowContrastDark,
    paddingHorizontal: gStyle.layouts.l,
    paddingVertical: gStyle.layouts.m,
  },
  buttonTextStyle: {
    fontSize: 14,
  },
});
