import React from 'react';
import { SafeArea, Text } from './common';
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import { gStyle } from './styles';
import Image from 'react-native-fast-image';

interface AvatarViewProps {
  items: AvatarProps[];
}
export const AvatarView = ({ items }: AvatarViewProps) => {
  return (
    <SafeArea style={styles.display}>
      <Text variation="h1">آواتار</Text>
      <Text variation="b1">یک عکس برای خودت انتخاب کن</Text>
      <ScrollView>
        <View style={styles.container}>
          {items.map((i) => (
            <Avatar {...i} />
          ))}
        </View>
      </ScrollView>
    </SafeArea>
  );
};

export interface AvatarProps {
  image: string;
  onSelect: (image: string) => void;
}
const Avatar = ({ image, onSelect }: AvatarProps) => {
  return (
    <TouchableOpacity
      onPress={() => onSelect(image)}
      style={styles.avatarContainer}>
      <Image
        source={{ uri: image }}
        style={styles.image}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

const AVATAR_WIDTH = 120;

const styles = StyleSheet.create({
  display: {
    padding: gStyle.layouts.l,
  },
  container: {
    flexWrap: 'wrap-reverse',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  avatarContainer: {
    margin: gStyle.layouts.l,
    marginVertical: gStyle.layouts.m,
    width: AVATAR_WIDTH,
    height: AVATAR_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    borderRadius: AVATAR_WIDTH,
    backgroundColor: 'white',
    ...gStyle.shadows.center,
    ...gStyle.shadows.small,
  },
  image: {
    width: '80%',
    height: '80%',
  },
});
