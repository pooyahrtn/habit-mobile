import React from 'react';
import { View } from 'react-native';
import { SafeArea, Text, TitleDevider, TextInput } from '../common';
import LoginImage from '../../assets/images/login/login.svg';
import styles from './login.styles';
import * as locale from '../../locale';

interface LoginViewProps {
  phoneNumber?: string;
  onPhoneChange: (value: string) => void;
  onPrivacyPressed: () => void;
}
export const LoginView = (props: LoginViewProps) => {
  const { phoneNumber, onPhoneChange, onPrivacyPressed } = props;
  return (
    <SafeArea style={styles.display}>
      <View style={styles.headerContainer}>
        <View style={styles.headerImageContainer}>
          <LoginImage height="100%" width="100%" />
        </View>
        <View style={styles.headerTextContainer}>
          <Text variation="h1">{locale.login.title}</Text>
          <Text variation="b1" style={styles.headerDescriptionText}>
            {locale.login.explanation}
          </Text>
        </View>
      </View>
      <Text variation="link" onPress={onPrivacyPressed}>
        {locale.login.privacyPolicy}
      </Text>
      <TitleDevider title={locale.login.withPhone} />
      <TextInput
        placeholder={locale.login.enterPhonePlaceHolder}
        value={phoneNumber}
        keyboardType="phone-pad"
        onChangeText={onPhoneChange}
        autoFocus={true}
        style={styles.inputStyle}
        maxLength={11}
      />
    </SafeArea>
  );
};
