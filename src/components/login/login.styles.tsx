import { StyleSheet } from 'react-native';
import { gStyle } from '../styles';

export default StyleSheet.create({
  display: {
    paddingHorizontal: gStyle.layouts.l,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingVertical: gStyle.layouts.l,
  },
  headerImageContainer: {
    flex: 1,
    aspectRatio: 1,
  },
  headerTextContainer: {
    flex: 1,
  },
  headerDescriptionText: {
    marginVertical: gStyle.layouts.m,
  },
  inputStyle: {
    textAlign: 'center',
  },
});
