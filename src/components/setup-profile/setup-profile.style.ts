import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export default StyleSheet.create({
  display: {
    paddingHorizontal: gStyle.layouts.l,
  },
  headerContainer: {
    paddingVertical: gStyle.layouts.m,
    flexDirection: 'row-reverse',
  },
  headerImage: {
    width: '25%',
    aspectRatio: 1 / 2,
  },
  headerTexts: {
    flex: 1,
    marginStart: gStyle.layouts.l,
  },
  headerTitle: {
    marginBottom: gStyle.layouts.m,
  },
  devider: {
    marginVertical: gStyle.layouts.s,
  },
  placeHolders: {
    marginVertical: gStyle.layouts.m,
  },
  groupStyle: {
    marginVertical: gStyle.layouts.m,
  },
  confirmStyle: {
    marginTop: gStyle.layouts.l,
  },
});
