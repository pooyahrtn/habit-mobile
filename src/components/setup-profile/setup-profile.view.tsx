import React from 'react';
import { View, ScrollView } from 'react-native';
import {
  SafeArea,
  Text,
  TitleDevider,
  TextInput,
  GroupButton,
  Button,
} from '../common';
import styles from './setup-profile.style';
import ProfileImage from '../../assets/images/login/profile.svg';
import * as locale from '../../locale';
import { Sex } from '@graphql/types/graphql-global-types';
import { SetCallback } from '@utils/types';

interface SetupProfileProps {
  selectedSex: Sex;
  onSexChange: SetCallback<Sex>;
  username: string;
  onUsernameChange: SetCallback<string>;
  age: string;
  onAgeChange: SetCallback<string>;
  loadingSetProfile: boolean;
  onConfirmPressed: () => void;
  disabled: boolean;
}
export const SetupProfile = (props: SetupProfileProps) => {
  const {
    selectedSex,
    onSexChange,
    username,
    onUsernameChange,
    age,
    onAgeChange,
    loadingSetProfile,
    onConfirmPressed,
    disabled,
  } = props;
  return (
    <SafeArea style={styles.display}>
      <ScrollView>
        <View style={styles.headerContainer}>
          <View style={styles.headerTexts}>
            <Text variation="h1" style={styles.headerTitle}>
              {locale.setupProfile.title}
            </Text>
            <Text variation="b1">{locale.setupProfile.headerBody}</Text>
          </View>
          <View style={styles.headerImage}>
            <ProfileImage width="100%" height="100%" />
          </View>
        </View>
        <TitleDevider
          title={locale.setupProfile.username}
          style={styles.devider}
        />
        <Text variation="b1">{locale.setupProfile.usernameDetail}</Text>
        <Text variation="b2">{locale.setupProfile.usernameHint}</Text>
        <TextInput
          placeholder={locale.setupProfile.usernamePlaceholder}
          containerStyle={styles.placeHolders}
          value={username}
          maxLength={40}
          onChangeText={onUsernameChange}
        />
        <TitleDevider title={locale.setupProfile.age} style={styles.devider} />
        <Text variation="b1">چند سالته؟</Text>
        <TextInput
          placeholder={locale.setupProfile.agePlaceholder}
          unit={locale.setupProfile.year}
          containerStyle={styles.placeHolders}
          value={age}
          maxLength={3}
          keyboardType="number-pad"
          onChangeText={onAgeChange}
        />
        <TitleDevider title={locale.setupProfile.sex} style={styles.devider} />
        <GroupButton
          containerStyle={styles.groupStyle}
          items={[
            {
              key: Sex.Female,
              value: locale.setupProfile.female,
            },
            {
              key: Sex.Male,
              value: locale.setupProfile.male,
            },
            {
              key: Sex.Other,
              value: locale.setupProfile.other,
            },
          ]}
          selectedKey={selectedSex}
          onChange={onSexChange as any}
        />
        <Button
          style={styles.confirmStyle}
          loading={loadingSetProfile}
          onPress={onConfirmPressed}
          disabled={disabled}
          content={{
            iconEnd: 'md-arrow-round-forward',
            text: locale.actions.confirm,
          }}
        />
      </ScrollView>
    </SafeArea>
  );
};
