import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import Image from 'react-native-fast-image';
import { Text } from '../common';
import { gStyle } from '@components/styles';
import { Toughness } from '@graphql/types/graphql-global-types';
import { ToughnessView } from './toughness.view';

export interface ChallengeListItemProps {
  imageURI: string;
  name: string;
  goal: string;
  id: string;
  toughness: Toughness;
  onPress: () => void;
}

export const ChallengeListItem = (props: ChallengeListItemProps) => {
  const { imageURI, name, goal, toughness, onPress } = props;
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.imageContainer}>
        <Image
          source={{ uri: imageURI }}
          resizeMode="contain"
          style={styles.image}
        />
      </View>

      <View style={styles.content}>
        <Text variation="h2">{name}</Text>
        <Text variation="b1">{goal}</Text>
        <ToughnessView toughness={toughness} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',

    marginVertical: gStyle.layouts.m,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  imageContainer: {
    marginTop: gStyle.layouts.m,
    width: 60,
    aspectRatio: 1,
    overflow: 'hidden',
    borderRadius: 30,
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
    marginEnd: gStyle.layouts.m,
    borderBottomWidth: 1,
    borderColor: gStyle.colors.lowContrastDark,
    paddingBottom: gStyle.layouts.m,
  },
});
