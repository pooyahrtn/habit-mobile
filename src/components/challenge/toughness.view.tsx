import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from '../common';
import { Toughness } from '@graphql/types/graphql-global-types';
import { gStyle } from '@components/styles';
import * as locale from '../../locale';

export interface ToughnessViewProps {
  toughness: Toughness;
}
export const ToughnessView = (props: ToughnessViewProps) => {
  const { toughness } = props;
  return (
    <View style={styles.container}>
      <Text variation="b3">
        {toughness === Toughness.Medium
          ? locale.toughness.medium
          : toughness === Toughness.Hard
          ? locale.toughness.hard
          : locale.toughness.easy}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    borderWidth: 1,
    borderColor: gStyle.colors.lowContrastDark,
    marginVertical: gStyle.layouts.s,
  },
});
