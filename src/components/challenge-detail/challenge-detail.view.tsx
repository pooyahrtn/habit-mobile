import React from 'react';
import { StyleSheet, View, Image, ScrollView } from 'react-native';
import {
  SafeArea,
  Text,
  IconButton,
  IconText,
  Button,
} from '@components/common';
import { Challenges_challenges } from '@graphql/queries/__generated__/Challenges';
import { gStyle } from '@components/styles';
import { ToughnessView } from '@components/challenge/toughness.view';
const LeftImage = require('../../assets/images/decorator/left.png');
import * as locale from '../../locale';
import { CannotAcceptMoreView } from '@components/CannotAcceptMore';

interface ChallengeDetailViewProps {
  variation: 'mine' | 'new' | 'show';
  challenge: Challenges_challenges;
  onBackPressed: () => void;
  onConfirmPressed: () => void;
  onDeletePressed: () => void;
  loading: boolean;
  onMorePressed: () => void;
}
export const ChallengeDetailView = (props: ChallengeDetailViewProps) => {
  const {
    challenge: { name, toughness, stats, description, goal, repeatCount },
    onBackPressed,
    onConfirmPressed,
    variation,
    onDeletePressed,
    loading,
    onMorePressed,
  } = props;
  return (
    <SafeArea style={styles.screen}>
      <View style={styles.header}>
        <IconButton onPress={onBackPressed} />
        <IconButton
          onPress={onMorePressed}
          iconName="md-more"
          containerStyle={styles.moreButton}
        />
        <Text variation="h2" style={styles.headerText}>
          {name}
        </Text>
      </View>
      <ScrollView style={styles.scrollView}>
        <View style={styles.contentWithImage}>
          <Image
            source={LeftImage}
            style={styles.leftImage}
            resizeMode="contain"
          />
          <View style={styles.content}>
            <ToughnessView toughness={toughness} />
            <IconText
              iconName="md-people"
              containerStyle={styles.statsContainer}
              color={gStyle.colors.lowContrastDark}
              text={locale.challenges.countEnroll(stats.enrolledCount)}
            />
            <IconText
              color={gStyle.colors.lowContrastDark}
              containerStyle={styles.statsContainer}
              iconName="md-checkmark"
              text={locale.challenges.countSuccess(stats.successCount)}
            />
            <Text variation="b1" style={styles.descriptionText}>
              {description}
            </Text>
            <IconText
              iconName="md-flag"
              text={locale.challenges.goal + ':\n' + goal}
            />
            <IconText
              iconName="md-calendar"
              text={
                locale.challenges.repeat +
                ':\n' +
                locale.challenges.perWeek(repeatCount)
              }
            />
          </View>
        </View>
      </ScrollView>
      {variation === 'new' && (
        <Button
          style={styles.confirmButton}
          content={{ text: 'شروع!' }}
          onPress={onConfirmPressed}
        />
      )}
      {variation === 'mine' && (
        <Button
          style={styles.deleteButtton}
          content={{ text: 'حذف چالش' }}
          onPress={onDeletePressed}
          loading={loading}
        />
      )}
      {variation === 'show' && <CannotAcceptMoreView />}
    </SafeArea>
  );
};

const styles = StyleSheet.create({
  screen: {},
  header: {
    flexDirection: 'row',
    marginVertical: gStyle.layouts.l,
    alignItems: 'center',
    paddingHorizontal: gStyle.layouts.l,
  },
  headerText: { flex: 1 },
  moreButton: {
    marginHorizontal: gStyle.layouts.l,
  },
  contentWithImage: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: gStyle.layouts.l,
  },
  leftImage: {
    width: 70,
    height: 260,
  },
  statsContainer: {
    marginBottom: 0,
  },
  descriptionText: {
    marginVertical: gStyle.layouts.l,
  },
  scrollView: {
    flex: 1,
  },
  confirmButton: {
    margin: gStyle.layouts.l,
  },
  deleteButtton: {
    margin: gStyle.layouts.l,
    backgroundColor: 'rgb(140,40,40)',
  },
});
