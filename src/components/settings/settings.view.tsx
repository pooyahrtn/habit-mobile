import React from 'react';
import { View, ScrollView, Switch, TouchableOpacity } from 'react-native';
import { SafeArea, TitleDevider, IconButton, Button } from '@components/common';
import { SetCallback } from '@utils/types';
import { Sex } from '@graphql/types/graphql-global-types';
import styles from './settings.styles';
import { Text, TextInput, GroupButton } from '@components/common';
import Image from 'react-native-fast-image';
import * as locale from '../../locale';

export interface SettingViewProps {
  onAvatarPressed: () => void;
  onSavePressed: () => void;
  onClosePressed: () => void;
  image: string;
  username: string;
  setUsername: SetCallback<string>;
  age: string;
  onAgeChange: SetCallback<string>;
  selectedSex: Sex;
  onSexChange: SetCallback<Sex>;
  notifications: boolean;
  setNotifications: SetCallback<boolean>;
}

export const SettingView = ({
  username,
  setUsername,
  age,
  onAgeChange,
  selectedSex,
  onSexChange,
  onClosePressed,
  image,
  notifications,
  setNotifications,
  onSavePressed,
  onAvatarPressed,
}: SettingViewProps) => {
  return (
    <SafeArea style={styles.container}>
      <View style={styles.headerContainer}>
        <Button
          content={{
            text: 'ذخیره',
            iconEnd: 'md-checkmark',
            color: 'primary',
          }}
          onPress={onSavePressed}
          style={styles.saveButtonContainer}
        />
        <IconButton onPress={onClosePressed} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text variation="h2">حساب کاربری</Text>
        <TouchableOpacity
          style={styles.avatarContainer}
          onPress={onAvatarPressed}>
          <View style={styles.avatarImageContainer}>
            <Image
              source={{ uri: image }}
              style={styles.avatarImage}
              resizeMode="contain"
            />
          </View>
          <Text variation="b2">تغییر تصویر</Text>
        </TouchableOpacity>

        <TitleDevider
          title={locale.setupProfile.username}
          style={styles.devider}
        />
        <Text variation="b1">{locale.setupProfile.usernameDetail}</Text>
        <Text variation="b2">{locale.setupProfile.usernameHint}</Text>
        <TextInput
          placeholder={locale.setupProfile.usernamePlaceholder}
          containerStyle={styles.placeHolders}
          value={username}
          maxLength={40}
          onChangeText={setUsername}
        />
        <TitleDevider title={locale.setupProfile.age} style={styles.devider} />
        <Text variation="b1">چند سالته؟</Text>
        <TextInput
          placeholder={locale.setupProfile.agePlaceholder}
          unit={locale.setupProfile.year}
          containerStyle={styles.placeHolders}
          value={age}
          maxLength={3}
          keyboardType="number-pad"
          onChangeText={onAgeChange}
        />
        <TitleDevider title={locale.setupProfile.sex} style={styles.devider} />
        <GroupButton
          containerStyle={styles.groupStyle}
          items={[
            {
              key: Sex.Female,
              value: locale.setupProfile.female,
            },
            {
              key: Sex.Male,
              value: locale.setupProfile.male,
            },
            {
              key: Sex.Other,
              value: locale.setupProfile.other,
            },
          ]}
          selectedKey={selectedSex}
          onChange={onSexChange as any}
        />
        <Text variation="h2" style={styles.title}>
          تنظیمات
        </Text>
        <TitleDevider
          title={locale.setupProfile.notification}
          style={styles.devider}
        />
        <View style={styles.notificationRow}>
          <Switch value={notifications} onValueChange={setNotifications} />
          <Text variation="b1" style={styles.notificattionText}>
            {locale.setupProfile.notificationDetil}
          </Text>
        </View>
        <View style={styles.escape} />
      </ScrollView>
    </SafeArea>
  );
};
