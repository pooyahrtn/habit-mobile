import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';
const AVATAR_SIZE = 100;

export default StyleSheet.create({
  container: {
    paddingHorizontal: gStyle.layouts.l,
  },
  headerContainer: {
    paddingTop: gStyle.layouts.l,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerImage: {
    width: '25%',
    aspectRatio: 1 / 2,
  },
  headerTexts: {
    flex: 1,
    marginEnd: gStyle.layouts.l,
  },
  headerTitle: {
    marginBottom: gStyle.layouts.m,
  },
  devider: {
    marginVertical: gStyle.layouts.s,
  },
  placeHolders: {
    marginVertical: gStyle.layouts.m,
  },
  groupStyle: {
    marginVertical: gStyle.layouts.m,
  },
  confirmStyle: {
    marginTop: gStyle.layouts.l,
  },
  saveButtonContainer: {
    paddingHorizontal: gStyle.layouts.m,
    minHeight: 40,
    backgroundColor: gStyle.colors.background,
    shadowOpacity: 0,
    elevation: 0,
    borderWidth: 1,
    borderColor: gStyle.colors.primary,
  },
  avatarImageContainer: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE,
    // overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    margin: gStyle.layouts.m,
    marginTop: gStyle.layouts.l,
    backgroundColor: 'white',
    ...gStyle.shadows.center,
    ...gStyle.shadows.small,
  },
  avatarImage: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE,
  },
  avatarContainer: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginBottom: gStyle.layouts.l,
  },
  title: {
    marginVertical: gStyle.layouts.m,
  },
  notificationRow: {
    flexDirection: 'row',
  },
  notificattionText: {
    flex: 1,
  },
  escape: {
    height: 100,
  },
});
