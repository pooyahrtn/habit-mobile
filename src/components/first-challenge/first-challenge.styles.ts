import { StyleSheet } from 'react-native';
import { gStyle } from '@components/styles';

export default StyleSheet.create({
  display: {
    paddingHorizontal: gStyle.layouts.l,
  },
  explanationText: {
    marginTop: gStyle.layouts.m,
  },
  content: {
    flex: 1,
    marginTop: gStyle.layouts.l,
  },
  laterText: { alignSelf: 'flex-start', marginVertical: gStyle.layouts.l },
});
