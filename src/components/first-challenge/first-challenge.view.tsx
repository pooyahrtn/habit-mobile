import React from 'react';
import { FlatList } from 'react-native';
import { SafeArea, Text } from '@components/common';
import styles from './first-challenge.styles';
import {
  ChallengeListItemProps,
  ChallengeListItem,
} from '../challenge/challenge-list-item';
import * as locale from '../../locale';

interface FirstChallengeViewProps {
  loading: boolean;
  items: ChallengeListItemProps[];
  onSkipPressed: () => void;
}
export const FirstChallengeView = (props: FirstChallengeViewProps) => {
  const { items, onSkipPressed } = props;
  return (
    <SafeArea style={styles.display}>
      <Text variation="h1">{locale.firstChallenge.title}</Text>
      <Text variation="b1" style={styles.explanationText}>
        {locale.firstChallenge.explanation}
      </Text>
      <FlatList
        style={styles.content}
        data={items}
        renderItem={({ item }) => <ChallengeListItem {...item} />}
      />
      <Text variation="link" style={styles.laterText} onPress={onSkipPressed}>
        {locale.firstChallenge.pickupLater}
      </Text>
    </SafeArea>
  );
};
