import React from 'react';
import { View, StyleSheet } from 'react-native';
import { gStyle } from './styles';
import { Text } from './common';
import * as locale from '../locale';

export const CannotAcceptMoreView = () => {
  return (
    <View style={styles.cannotAcceptContainer}>
      <Text style={styles.cannotText} variation="b1">
        {locale.challenges.cannotAcceptMore}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  cannotAcceptContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: gStyle.colors.primaryLighter,
    ...gStyle.shadows.center,
    ...gStyle.shadows.small,
  },
  cannotText: {
    margin: gStyle.layouts.l,
  },
});
