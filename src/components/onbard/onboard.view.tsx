import React, { forwardRef } from 'react';
import { Dimensions, View, StyleSheet } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Text, SafeArea, Button } from '../common';
import { gStyle } from '../styles';

export interface OnboardItemProps {
  key: string;
  image: any;
  title: string;
  body: string;
}

export interface OnboardViewProps {
  items: OnboardItemProps[];
  activeSlideNumber: number;
  onSnapToSlide: (index: number) => void;
  onNextButtonPressed: () => void;
}
export const OnboardView = forwardRef((props: OnboardViewProps, ref: any) => {
  const {
    items,
    onSnapToSlide,
    activeSlideNumber,
    onNextButtonPressed,
  } = props;
  return (
    <SafeArea padding="horizontal">
      <Carousel
        data={items}
        renderItem={({ item }) => <IntroItem {...item} />}
        sliderWidth={screenWidth}
        itemWidth={screenWidth}
        onSnapToItem={onSnapToSlide}
        ref={ref}
      />
      <View style={styles.bottomRowContainer}>
        <Pagination
          dotsLength={items.length}
          activeDotIndex={activeSlideNumber}
          containerStyle={styles.paginationContainerStyle}
          dotStyle={styles.paginationDotStyle}
          dotContainerStyle={styles.paginatorDotContainer}
          inactiveDotStyle={styles.paginationDotInactiveStyle}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
        <Button
          backgroundColor="primary"
          onPress={onNextButtonPressed}
          content={{
            text: 'خب',
            color: 'highContrastLight',
            iconEnd: 'md-arrow-round-forward',
          }}
        />
      </View>
    </SafeArea>
  );
});

const IntroItem = (props: OnboardItemProps) => {
  const { image, title, body } = props;
  return (
    <View style={styles.carousalItemContainer}>
      <View style={styles.carousalImageContainer}>{image}</View>
      <View style={styles.carousalTextContainer}>
        <Text variation="h1">{title}</Text>
        <Text variation="b1" style={styles.carousalBodyText}>
          {body}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  carousalItemContainer: {
    height: '90%',
    width: '100%',
    paddingTop: gStyle.layouts.l,
  },
  carousalImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: '40%',
    marginVertical: gStyle.layouts.xl,
  },
  carousalTextContainer: {
    flex: 1,
    paddingHorizontal: gStyle.layouts.l,
  },
  carousalBodyText: {
    marginTop: gStyle.layouts.l,
  },
  paginationDotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  paginatorDotContainer: {
    margin: 0,
    padding: 0,
    width: 5,
  },
  paginationDotInactiveStyle: {
    // Define styles for inactive dots here
    margin: 0,
  },
  paginationContainerStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: 0,
  },
  bottomRowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: gStyle.layouts.l,
    paddingHorizontal: gStyle.layouts.l,
  },
});

const screenWidth = Dimensions.get('screen').width;
