import React from 'react';
import { View } from 'react-native';
import { SafeArea, Text, TitleDevider, TextInput } from '../common';
import LoginImage from '../../assets/images/login/confirm.svg';
import styles from './confirm-phone.styles';
import * as locale from '../../locale';

interface LoginViewProps {
  phoneNumber: string;
  confirmCode: string;
  onConfirmCodeChange: (value: string) => void;
  error?: string;
  showResend: boolean;
  onResendPressed: () => void;
  onChangePhonePressed: () => void;
}
export const ConfirmView = (props: LoginViewProps) => {
  const {
    confirmCode,
    onConfirmCodeChange,
    phoneNumber,
    error,
    showResend,
    onChangePhonePressed,
    onResendPressed,
  } = props;
  return (
    <SafeArea style={styles.display}>
      <View style={styles.headerContainer}>
        <View style={styles.headerImageContainer}>
          <LoginImage height="100%" width="100%" />
        </View>
        <View style={styles.headerTextContainer} />
      </View>
      <Text variation="b1">{locale.login.confirmSent(phoneNumber)}</Text>
      <TitleDevider title={locale.login.confirmCode} />
      <TextInput
        placeholder={locale.login.confirmCodePlaceHolder}
        value={confirmCode}
        keyboardType="phone-pad"
        onChangeText={onConfirmCodeChange}
        style={styles.inputStyle}
        maxLength={5}
        autoFocus={true}
        error={error}
      />
      <View style={styles.bottomRow}>
        <Text variation="link" onPress={onChangePhonePressed}>
          {locale.login.changePhone}
        </Text>
        {showResend && (
          <Text variation="link" onPress={onResendPressed}>
            {locale.login.resendCode}
          </Text>
        )}
      </View>
    </SafeArea>
  );
};
