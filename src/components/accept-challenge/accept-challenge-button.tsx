import React from 'react';
import {
  State,
  PanGestureHandler,
  TapGestureHandler,
} from 'react-native-gesture-handler';
import Animated, {
  Value,
  cond,
  eq,
  interpolate,
  call,
  useCode,
} from 'react-native-reanimated';
import { Vibration, StyleSheet, Platform } from 'react-native';
import { Icon } from '@components/common';
import {
  onGestureEvent,
  withTransition,
  bInterpolateColor,
} from 'react-native-redash';
import { gStyle } from '@components/styles';
const vibrate = () =>
  Platform.OS === 'android' && Vibration.vibrate([0, 20, 100, 20]);

interface AcceptChallengeButtonProps {
  onDone: () => void;
}
export const AcceptChallengeButton = ({
  onDone,
}: AcceptChallengeButtonProps) => {
  const state = new Value(State.UNDETERMINED);
  const gestureHandler = onGestureEvent({ state });
  const isActive = eq(state, State.BEGAN);
  const duration = cond(isActive, 1500, 250);
  const progress = withTransition(isActive, { duration });
  let done = false;
  const scale = interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [1, 1.5],
  });
  const color = bInterpolateColor(
    progress,
    'white',
    gStyle.colors.primaryLighter,
  );
  const Handler = container();
  useCode(
    () =>
      cond(
        eq(progress, 1),
        call([], () => {
          if (!done) {
            vibrate();
            onDone();
          }
          done = true;
        }),
      ),
    [progress],
  );
  return (
    <Handler {...gestureHandler}>
      <Animated.View
        style={[
          styles.container,
          { transform: [{ scale }], backgroundColor: color },
        ]}>
        <Icon name="md-finger-print" size={40} />
      </Animated.View>
    </Handler>
  );
};

const container = (): any =>
  Platform.OS === 'android' ? PanGestureHandler : TapGestureHandler;

const BUTTON_SIZE = 100;
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: BUTTON_SIZE / 2,
    width: BUTTON_SIZE,
    height: BUTTON_SIZE,
    justifyContent: 'center',
    alignItems: 'center',
    ...gStyle.shadows.center,
    ...gStyle.shadows.xsmall,
  },
});
