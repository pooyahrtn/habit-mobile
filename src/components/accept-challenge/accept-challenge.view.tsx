import React from 'react';
import { StyleSheet, View } from 'react-native';
import { SafeArea, Text, IconButton } from '@components/common';
import ContractImage from '../../assets/images/challenge/contract.svg';
import Arrow from '../../assets/images/arrow.svg';
import * as locale from '../../locale';
import { gStyle } from '@components/styles';
import { AcceptChallengeButton } from './accept-challenge-button';

interface AcceptChallengeViewProps {
  challengeName: string;
  onClosePressed: () => void;
  onConfirmed: () => void;
}
export const AcceptChallengeView = ({
  challengeName,
  onClosePressed,
  onConfirmed,
}: AcceptChallengeViewProps) => {
  return (
    <SafeArea style={styles.screen}>
      <View style={styles.headerContainer}>
        <IconButton onPress={onClosePressed} />
        <Text variation="h1">{locale.challenges.contract}</Text>
      </View>
      <View style={styles.imageContiner}>
        <ContractImage width="100%" height="100%" />
      </View>
      <Text variation="b1" style={styles.contractText}>
        {locale.challenges.promise(challengeName)}
      </Text>
      <View style={styles.acceptButtonContainer}>
        <View style={styles.tutContainer}>
          <Text variation="b2" style={styles.tutText}>
            دکمه‌ی زیر رو نگه دار
          </Text>
          <Arrow />
        </View>
        <AcceptChallengeButton onDone={onConfirmed} />
      </View>
    </SafeArea>
  );
};

const TUT_WIDTH = 200;

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: gStyle.layouts.l,
  },
  headerContainer: {
    paddingVertical: gStyle.layouts.l,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContiner: {
    width: '60%',
    aspectRatio: 1,
    alignSelf: 'center',
  },
  contractText: {
    textAlign: 'center',
    marginVertical: gStyle.layouts.l,
    fontSize: 20,
  },
  acceptButtonContainer: {
    flex: 1,
    alignItems: 'center',
  },
  tutContainer: {
    marginBottom: gStyle.layouts.xl,
    left: TUT_WIDTH / 2,
    width: TUT_WIDTH,
    position: 'relative',
  },
  tutText: {
    textAlign: 'left',
    fontWeight: 'bold',
    marginVertical: gStyle.layouts.s,
  },
});
