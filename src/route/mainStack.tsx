import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { MainStackParams } from './params';
import App from './tabs';
import { ChallengeDetailPage } from '@pages/challenge-detail.page';
import { AcceptChallengePage } from '@pages/accept-challenge.page';
import { SettingPage } from '@pages/settings.page';
import { AvatarPage } from '@pages/avatars.page';
import { NewChallengePage } from '@pages/new-challenge.page';
import { ChallengeCreatedView } from '@pages/challenge-created.page';

const Stack = createStackNavigator<MainStackParams>();

export default () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="tabs" component={App} />
      <Stack.Screen name="challengeDeta" component={ChallengeDetailPage} />
      <Stack.Screen name="acceptChallenge" component={AcceptChallengePage} />
      <Stack.Screen name="settings" component={SettingPage} />
      <Stack.Screen name="avatars" component={AvatarPage} />
      <Stack.Screen name="newChallenge" component={NewChallengePage} />
      <Stack.Screen name="challengeCreated" component={ChallengeCreatedView} />
    </Stack.Navigator>
  );
};
