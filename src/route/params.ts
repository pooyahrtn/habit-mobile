import { Challenges_challenges } from '@graphql/queries/__generated__/Challenges';

export type EntryStackParams = {
  onboard: {};
  login: {};
  confirmPhone: {
    phoneNumber: string;
  };
  setupProfile: {};
  pickFirstChallenge: {};
  app: {};
  forceUpdate: {};
};

export type TabNavigationParams = {
  home: {};
  explore: {};
  leaderboard: {};
};

export type MainStackParams = {
  tabs: {};
  challengeDeta: {
    challenge: Challenges_challenges;
    mine: boolean;
  };
  acceptChallenge: {
    challenge: Challenges_challenges;
  };
  settings: {};
  avatars: {};
  newChallenge: {};
  challengeCreated: {};
};
