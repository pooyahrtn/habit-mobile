import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AppEnterState } from '@graphql/types/graphql-global-types';
import { LoginPage } from '@pages/login/login.page';
import { SetupPage } from '@pages/setup-profile.page';
import OnboardPage from '@pages/onboard/onboard.page';
import { ConfirmPage } from '@pages/confirm-phone.page';
import { EntryStackParams } from './params';
import { FirstChallengePage } from '@pages/first-challenge.page';
import App from './mainStack';
import { ForceUpdatePage } from '@pages/force-update';

const Stack = createStackNavigator<EntryStackParams>();

export interface EntryStackProps {
  appEnterMode: AppEnterState;
}
export default (props: EntryStackProps) => {
  const { appEnterMode } = props;

  return (
    <Stack.Navigator headerMode="none">
      {appEnterMode === AppEnterState.JUST_ENTER && (
        <Stack.Screen name="onboard" component={OnboardPage} />
      )}
      {appEnterMode === AppEnterState.WAIT_FOR_LOGIN && (
        <>
          <Stack.Screen name="login" component={LoginPage} />
          <Stack.Screen name="confirmPhone" component={ConfirmPage} />
        </>
      )}
      {appEnterMode === AppEnterState.WAIT_FOR_PROFILE && (
        <Stack.Screen name="setupProfile" component={SetupPage} />
      )}
      {appEnterMode === AppEnterState.FORCE_UPDATE && (
        <Stack.Screen name="forceUpdate" component={ForceUpdatePage} />
      )}
      {appEnterMode === AppEnterState.PICK_FIRST_CHALLENGE && (
        <Stack.Screen
          name="pickFirstChallenge"
          component={FirstChallengePage}
        />
      )}
      {appEnterMode === AppEnterState.LOGGED_IN && (
        <Stack.Screen name="app" component={App} />
      )}
    </Stack.Navigator>
  );
};
