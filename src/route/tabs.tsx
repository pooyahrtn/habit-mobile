import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TabNavigationParams } from './params';
import { IonNames, TabIcon } from '@components/common';
import { HomePage } from '@pages/home.page';
import { ExplorePage } from '@pages/explore.page';
import { LeaderboardPage } from '@pages/leaderboard.page';

const Tabs = createBottomTabNavigator<TabNavigationParams>();

export default () => {
  return (
    <Tabs.Navigator
      screenOptions={({ route }: { route: any }) => getTabIcon(route.name)}
      tabBarOptions={{
        showLabel: false,
        tabStyle: {
          // backgroundColor: theme.colors.primary,
          borderWidth: 0,
        },
      }}
      initialRouteName="home">
      <Tabs.Screen name="leaderboard" component={LeaderboardPage} />
      <Tabs.Screen name="explore" component={ExplorePage} />
      <Tabs.Screen name="home" component={HomePage} />
    </Tabs.Navigator>
  );
};

const getIconName = (routeName: keyof TabNavigationParams): IonNames => {
  switch (routeName) {
    case 'home':
      return 'md-home';
    case 'explore':
      return 'md-compass';
    case 'leaderboard':
      return 'md-ribbon';
  }
};

const getTabIcon = (routeName: keyof TabNavigationParams) => {
  return {
    tabBarIcon: ({ focused }: { focused: boolean }) => (
      <TabIcon
        name={getIconName(routeName)}
        active={focused}
        hasNotification={false}
      />
    ),
  };
};
