module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'graphql-tag',
    [
      'module-resolver',
      {
        root: ['./src'],
        // extensions: [
        //   '.ios.ts',
        //   '.android.ts',
        //   '.ts',
        //   '.ios.tsx',
        //   '.android.tsx',
        //   '.tsx',
        //   '.jsx',
        //   '.js',
        //   '.json',
        // ],
        alias: {
          '@components': './src/components',
          '@utils': './src/utils',
          '@pages': './src/pages',
          '@route': './src/route',
          '@graphql': './src/graphql',
          '@controller': './src/controller',
          '@packages': './src/packages',
        },
      },
    ],
  ],
};
