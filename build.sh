#!/bin/sh

echo 'Enter version code:'
read version
appName="Hapit"
buildFolder=./builds/"${version}"
androidFolder="${buildFolder}/android"
mkdir "${buildFolder}"
mkdir "${androidFolder}"

cd ./android
./gradlew clean
./gradlew assembleRelease
# ./gradlew bundleRelease

cd ..

mv ./android/app/build/outputs/apk/release/app-release.apk \
  "${androidFolder}/${appName}-V${version}.apk"

# mv ./android/app/build/outputs/bundle/release/app.aab \
#   "${androidFolder}/${appName}-V${version}.aab"
