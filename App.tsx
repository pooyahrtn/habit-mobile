import { enableScreens } from 'react-native-screens';

enableScreens();

export { default } from './src/app';
